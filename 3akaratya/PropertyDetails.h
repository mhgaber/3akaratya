//
//  PropertyDetails.h
//  3akaratya
//
//  Created by mhGaber on 7/30/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
#import "KIImagePager.h"
#import <GoogleMaps/GoogleMaps.h>
#import "PropertyDetailsModel.h"
#import "WYPopoverController.h"

@interface PropertyDetails : UITableViewController<KIImagePagerDataSource,KIImagePagerDelegate,WYPopoverControllerDelegate,UIActionSheetDelegate>
- (IBAction)showagentinfo:(id)sender;

@property(strong,nonatomic) NSString *propertyID;
@property(strong,nonatomic) NSMutableArray *propertyImagesGallery;
@property (weak, nonatomic) IBOutlet KIImagePager *propertyGallery;
@property (weak, nonatomic) IBOutlet UILabel *pricelabel;

@property (weak, nonatomic) IBOutlet UIButton *publisherthumb;

@property (weak, nonatomic) IBOutlet UILabel *propertyIDLabel;

@property (weak, nonatomic) IBOutlet UILabel *datelabel;
@property (weak, nonatomic) IBOutlet UITextView *propertyDescription;
@property (weak, nonatomic) IBOutlet GMSMapView *propertyLocation;
@property (weak, nonatomic) IBOutlet UILabel *roomslabel;

@property (weak, nonatomic) IBOutlet UILabel *arealabel;
@property (weak, nonatomic) IBOutlet UILabel *viewslabel;
- (IBAction)shareaction:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *floorslabel;
@property (weak, nonatomic) IBOutlet UILabel *yearlabel;
@property (weak, nonatomic) IBOutlet UILabel *unitslabel;
@property (weak, nonatomic) IBOutlet UILabel *propertytitle;
@property (weak, nonatomic) IBOutlet UIButton *followpublishertext;
@property(strong,nonatomic) PropertyDetailsModel *propertyDetails;
@property (weak, nonatomic) IBOutlet UIButton *followpublishericon;
@property(strong,nonatomic)    WYPopoverController* agentPopoverController;
- (IBAction)showmoreinfo:(id)sender;
@property(strong,nonatomic)    UIActionSheet *actionSheet;
- (IBAction)followpublisheraction:(id)sender;
- (IBAction)addtofavaction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addtofav;

@property (weak, nonatomic) IBOutlet UIButton *addtofavicon;
@property (weak, nonatomic) IBOutlet UILabel *datetitle;
@property (weak, nonatomic) IBOutlet UILabel *roomstitle;
@property (weak, nonatomic) IBOutlet UILabel *floorstitle;
@property (weak, nonatomic) IBOutlet UILabel *yearstitle;
@property (weak, nonatomic) IBOutlet UILabel *viewstitle;
@property (weak, nonatomic) IBOutlet UILabel *unitstitle;
@property (weak, nonatomic) IBOutlet UILabel *areatitle;
@property (weak, nonatomic) IBOutlet UILabel *desctitle;
@property (weak, nonatomic) IBOutlet UILabel *siteonmaptitle;
@property (weak, nonatomic) IBOutlet UIButton *askmoreinfo;

@property (weak, nonatomic) IBOutlet UIButton *foloowpublishertext;
@property (weak, nonatomic) IBOutlet UIButton *commentsonproperty;

@property (weak, nonatomic) IBOutlet UIButton *addfavbtn;

@property (weak, nonatomic) IBOutlet UILabel *addresslabel;

@property(strong,nonatomic) NSMutableArray *locationsArrr;

- (IBAction)facebookshare:(id)sender;

- (IBAction)tweetaction:(id)sender;




@end
