//
//  AppManager.m
//  3akaratya
//
//  Created by mhGaber on 8/10/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "AppManager.h"

@implementation AppManager

+ (id)sharedManager {
    static AppManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
-(void)setHomeList:(NSMutableArray *)homeList{
    _homeList=homeList;
}
-(void)setIsLoggedIn:(BOOL)isLoggedIn{
    _isLoggedIn=isLoggedIn;
}
-(void)setIsFirstTime:(BOOL)isFirstTime{
    _isFirstTime=isFirstTime;
}
-(void)setAccessToken:(NSString *)accessToken{
    _accessToken=accessToken;
}
-(void) setMessages:(NSMutableArray *)messages{
    _messages=messages;
}
-(void)setRequestsArr:(NSMutableArray *)requestsArr{
    _requestsArr=requestsArr;
}
-(void)setLocationsDefined:(NSMutableArray *)locationsDefined{
    _locationsDefined=locationsDefined;
}
-(void)setLocationsIDs:(NSMutableArray *)locationsIDs{
    _locationsIDs=locationsIDs;
}
-(void)setLocationsArr:(NSMutableArray *)locationsArr{
    _locationsArr=locationsArr;
}
-(void)setPropertyDetailsModel:(PropertyDetailsModel *)propertyDetailsModel{
    _propertyDetailsModel=propertyDetailsModel;
}
@end
