//
//  LoginViewController.h
//  3akaratya
//
//  Created by mhGaber on 8/8/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
- (IBAction)cancellogin:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
- (IBAction)dismisskeyboard:(id)sender;

- (IBAction)login:(id)sender;
@end
