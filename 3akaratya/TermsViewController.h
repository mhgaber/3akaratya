//
//  TermsViewController.h
//  3akaratya
//
//  Created by mhGaber on 7/23/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *termstextview;

- (IBAction)openmenu:(id)sender;

- (IBAction)backtermaction:(id)sender;

@end
