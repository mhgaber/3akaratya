//
//  MenuRow.h
//  3akaratya
//
//  Created by mhGaber on 7/11/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuRow : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titlerow;
@property (weak, nonatomic) IBOutlet UIButton *notificationbutton;

@end
