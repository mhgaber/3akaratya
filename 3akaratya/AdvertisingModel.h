//
//  AdvertisingModel.h
//  3akaratya
//
//  Created by mhGaber on 8/16/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdvertisingModel : NSObject
@property(strong,nonatomic) NSString *text;
@property(nonatomic) int advID;
@property(strong,nonatomic)NSString *targetURL;


@end
