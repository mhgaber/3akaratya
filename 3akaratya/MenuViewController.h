//
//  MenuViewController.h
//  3akaratya
//
//  Created by mhGaber on 7/11/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import <QuartzCore/QuartzCore.h>
@interface MenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(strong,nonatomic) NSMutableArray *menuitemsLog;
@property(strong,nonatomic) NSMutableArray *menuitemsnotlogged;

@property (weak, nonatomic) IBOutlet UITableView *menutable;

@end
