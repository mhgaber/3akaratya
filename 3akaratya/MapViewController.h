//
//  MapViewController.h
//  3akaratya
//
//  Created by mhGaber on 7/13/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CLLocationManager.h>
#import <CoreLocation/CoreLocation.h>
@interface MapViewController : UIViewController<CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet GMSMapView *mapproperties;
@property(strong,nonatomic)   CLLocationManager *locationManager;
@property(nonatomic) double lat;
@property(nonatomic) double lon;
@property(strong,nonatomic) NSMutableArray *propertiesLista;

- (IBAction)returntogrid:(id)sender;
@end
