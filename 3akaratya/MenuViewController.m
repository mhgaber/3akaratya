//
//  MenuViewController.m
//  3akaratya
//
//  Created by mhGaber on 7/11/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "MenuViewController.h"
#import "ECSlidingViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MenuRow.h"
#import "AppManager.h"
#import "LoginViewController.h"
#import "ViewController.h"
@implementation MenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
 //   if([[AppManager sharedManager]isLoggedIn])
    _menuitemsLog=[[NSMutableArray alloc]initWithObjects:@"الرئيسيه",@"عقاراتي",@"تفاصيل الاتصال",@"من اتابع",@"المتابعين",@"اعلانات",@"إشعارات",@"ترقيه الحساب",@"البنود",@"التواصل",@"تسجيل الخروج", nil];
   // else
        _menuitemsnotlogged=[[NSMutableArray alloc]initWithObjects:@"الرئيسيه",@"التسجيل",@"تسجيل الدخول",@"البنود",@"التواصل", nil];
    _menutable.delegate=self;
    _menutable.dataSource=self;
    [self.slidingViewController setAnchorRightRevealAmount:100.0f];

//    self.slidingViewController.underRightViewController = self.slidingViewController.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if([[AppManager sharedManager]isLoggedIn])
    return _menuitemsLog.count;
    
    else
      return   _menuitemsnotlogged.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"menurow";
    
    MenuRow *cell = [_menutable
                      dequeueReusableCellWithIdentifier:simpleTableIdentifier
                      forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[MenuRow alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    if([[AppManager sharedManager]isLoggedIn])
        
    cell.titlerow.text=[_menuitemsLog objectAtIndex:indexPath.row];
    
    else
    cell.titlerow.text=[_menuitemsnotlogged objectAtIndex:indexPath.row];
    
    cell.titlerow.font=[UIFont fontWithName:@"Tanseek M Medium" size:20];
    if([[AppManager sharedManager]isLoggedIn]){
    if(indexPath.row ==4||indexPath.row==5)
        cell.notificationbutton.hidden=NO;
    else
        cell.notificationbutton.hidden=YES;
    if(indexPath.row==3){
        cell.notificationbutton.hidden=NO;
           [cell.notificationbutton setBackgroundImage:nil forState:UIControlStateNormal];
    }
    }
    else
    cell.notificationbutton.hidden=YES;
    
    return  cell;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *identifier = nil;
    if([[AppManager sharedManager]isLoggedIn]){
    if(indexPath.row==0){
        identifier=@"homeid";
    }
        if(indexPath.row==2){
        identifier=@"contactdetailsid";
        }
    if(indexPath.row==8){
        identifier=@"termsstory";
    }
    if(indexPath.row==3){
        [[AppManager sharedManager]setIsFollowers:NO];
    identifier=@"followingstory";
    }
        if(indexPath.row==4){
            [[AppManager sharedManager]setIsFollowers:YES];
            identifier=@"followingstory";
        }
    if(indexPath.row==5){
    identifier=@"advertisingstoryboard";
    }
    if(indexPath.row==9){
    identifier=@"contactstoryboard";        
    }
    if(indexPath.row==10){
        [[AppManager sharedManager]setIsLoggedIn:NO];
        [[NSUserDefaults standardUserDefaults ]setObject:nil forKey:@"accessToken"];
        [[AppManager sharedManager]setAccessToken:nil];
        [_menutable reloadData];
        if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered){
            [self.slidingViewController anchorTopViewToLeftAnimated:YES];
        }
        else{
            [self.slidingViewController resetTopViewAnimated:YES];
        }
        identifier=@"homeid";
    }
}else {
            if(indexPath.row==0){
                identifier=@"homeid";
            }

    if(indexPath.row==2){
        
        identifier=@"logincontroller";
    }
            if(indexPath.row==1){
                if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered){
                    [self.slidingViewController anchorTopViewToLeftAnimated:YES];
                }
            
            }
            /*else{
                    [self.slidingViewController resetTopViewAnimated:YES];
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    LoginViewController *privacy = (LoginViewController*)[storyboard instantiateViewControllerWithIdentifier:@"logincontroller"];
                    [self.slidingViewController presentModalViewController:privacy animated:YES];
             //   [self.navigationController presentViewController:privacy animated:YES completion:nil];
            }
                
           */
            if(indexPath.row==3){
                    identifier=@"termsstory";
            }
            if(indexPath.row==4){
                identifier=@"contactstoryboard";
            }
                
                
        }
        
    if(identifier!=nil){
        
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
        [self.slidingViewController resetTopViewAnimated:YES];
    
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}




@end