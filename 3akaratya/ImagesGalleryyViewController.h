//
//  ImagesGalleryyViewController.h
//  3akaratya
//
//  Created by mhGaber on 9/25/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KIImagePager.h"
#import "PropertyDetailsModel.h"
@interface ImagesGalleryyViewController : UIViewController<KIImagePagerDelegate,KIImagePagerDataSource>
@property(strong,nonatomic)PropertyDetailsModel *propertyDetailsModel;
@property (weak, nonatomic) IBOutlet KIImagePager *imagesPaggers;
@end
