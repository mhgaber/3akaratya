//
//  FollowinViewController.h
//  3akaratya
//
//  Created by mhGaber on 8/3/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface FollowinViewController : UIViewController<SWTableViewCellDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *followingtableview;
- (IBAction)openmenuaction:(id)sender;
@property(strong,nonatomic)NSMutableArray *followingArr;
@property (weak, nonatomic) IBOutlet UILabel *headtitle;

@property (weak, nonatomic) IBOutlet UILabel *nodatalabel;

- (IBAction)backbtnaction:(id)sender;

@end
