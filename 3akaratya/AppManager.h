//
//  AppManager.h
//  3akaratya
//
//  Created by mhGaber on 8/10/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Property.h"
#import "PropertyDetailsModel.h"
@interface AppManager : NSObject
+ (id)sharedManager;
@property(strong,nonatomic)NSMutableArray *homeList;
-(void) setHomeList:(NSMutableArray *)homeList;
@property(nonatomic) BOOL isFirstTime;
-(void) setFirstTime:(BOOL)isFirst;
@property(nonatomic)BOOL isLoggedIn;
-(void) setIsLoggedIn:(BOOL)isLoggedIn;
@property(strong,nonatomic)NSString *accessToken;
-(void)setAccessToken:(NSString *)accessToken;
@property(nonatomic) BOOL isFollowers;
-(void)setIsFollowers:(BOOL)isFollowers;
@property(strong,nonatomic)NSMutableArray *messages;
-(void)setMessages:(NSMutableArray *)messages;
@property(strong,nonatomic) NSMutableArray *requestsArr;
-(void)setRequestsArr:(NSMutableArray *)requestsArr;
@property(strong, nonatomic) NSMutableArray *locationsDefined;
-(void)setLocationsDefined:(NSMutableArray *)locationsDefined;
@property(strong,nonatomic) NSMutableArray *locationsIDs;
-(void)setLocationsIDs:(NSMutableArray *)locationsIDs;
@property(strong,nonatomic) NSMutableArray *locationsArr;
-(void)setLocationsArr:(NSMutableArray *)locationsArr;
@property(strong,nonatomic) PropertyDetailsModel *propertyDetailsModel;
-(void) setPropertyDetailsModel:(PropertyDetailsModel *)propertyDetailsModel;

@end
