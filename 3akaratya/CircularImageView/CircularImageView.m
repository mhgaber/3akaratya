//
//  CircularImageView.m
//
//  Created by Johnny on 9/5/13.
//  Copyright (c) 2013 Johnny. All rights reserved.
//

#import "CircularImageView.h"
#import <QuartzCore/QuartzCore.h>

@implementation CircularImageView

- (instancetype)initWithCenter:(CGPoint)center radius:(CGFloat)radius
{
    CGRect frame = CGRectMake(center.x - radius, center.y - radius, 2 * radius, 2 * radius);
    if (self = [super initWithFrame:frame]) {
        _radius = radius;
        
        self.layer.borderWidth = 2.0;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.cornerRadius = _radius;
        self.layer.masksToBounds = YES;
    }
    
    return self;
}

- (void)setRadius:(CGFloat)radius
{
    _radius = radius;
    self.frame = CGRectMake(self.center.x - radius, self.center.y - radius, 2 * _radius, 2 * _radius);
    self.layer.cornerRadius = _radius;
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
    _borderWidth = borderWidth;
    self.layer.borderWidth = _borderWidth;
}

- (void)setBorderColor:(UIColor *)borderColor
{
    _borderColor = borderColor;
    self.layer.borderColor = _borderColor.CGColor;
}

- (void)setFrame:(CGRect)frame
{
    _radius = frame.size.width < frame.size.height ? frame.size.width / 2.0 : frame.size.height / 2.0;
    self.layer.cornerRadius = _radius;
    
    [super setFrame:CGRectMake(frame.origin.x, frame.origin.y, 2 * _radius, 2 * _radius)];
}

- (void)setBounds:(CGRect)bounds
{
    _radius = bounds.size.width < bounds.size.height ? bounds.size.width / 2.0 : bounds.size.height / 2.0;
    self.layer.cornerRadius = _radius;
        
    [super setBounds:CGRectMake(bounds.origin.x, bounds.origin.y, 2 * _radius, 2 * _radius)];
}

@end
