//
//  CircularImageView.h
//
//  Created by Johnny on 9/5/13.
//  Copyright (c) 2013 Johnny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircularImageView : UIImageView

@property (nonatomic, assign) CGFloat radius;
@property (nonatomic, assign) CGFloat borderWidth;
@property (nonatomic, strong) UIColor *borderColor;

- (instancetype)initWithCenter:(CGPoint)center radius:(CGFloat)radius;

@end
