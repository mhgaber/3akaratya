//
//  FollowinViewController.m
//  3akaratya
//
//  Created by mhGaber on 8/3/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "FollowinViewController.h"
#import "FollowingTableCell.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "AppManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFNetworking.h>
#import "XMLReader.h"
#import "FollowingModel.h"
@implementation FollowinViewController
-(void) viewDidLoad{
    _followingArr=[[NSMutableArray alloc]init];

    _followingtableview.dataSource=self;
    _followingtableview.delegate=self;
    if([[AppManager sharedManager]isFollowers]){
        [self fetchFollowers];
        _headtitle.text=@"المتابعين";
    }else {
    [self fetchFollowing];
        _headtitle.text=@"من أتابع";
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"followingcell";
    
    FollowingTableCell *cell = (FollowingTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                                                     forIndexPath:indexPath];
    if(![[AppManager sharedManager]isFollowers])
    cell.leftUtilityButtons = [self leftButtons];
    cell.delegate = self;
    FollowingModel *following=[_followingArr objectAtIndex:indexPath.row];
    cell.tradename.text=following.userName;
    
    return cell;

    
    
}
-(void)fetchFollowers{
    if([[AppManager sharedManager]accessToken]!=nil){
        NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
        NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetUserFollowings><info:AuthToken>%@</info:AuthToken></info:GetUserFollowings></soapenv:Body></soapenv:Envelope>",[[AppManager sharedManager]accessToken]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPMethod:@"POST"]; //GET
        
        [request addValue:@"http://info-est.com/GetUserFollowers" forHTTPHeaderField:@"SOAPAction"];
        
        [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:mProces];
        mProces.animationType=MBProgressHUDAnimationFade;
        mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
        [mProces show:YES];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
            
            NSLog(@"Success : Content : %@",responseObject);
            NSLog(@"Success : Content : %@",[operation responseString]);
            NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSError *error = nil;
            NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
            NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
            NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
            NSDictionary *dic3=[dic2 objectForKey:@"GetUserFollowersResponse"];
            NSDictionary *following=[dic3 objectForKey:@"GetUserFollowersResult"];
            NSDictionary *resultr=[following objectForKey:@"Result"];
            NSArray *resultsArr=[resultr objectForKey:@"string"];
            if(resultsArr !=nil &&resultsArr.count>0){

            [_followingArr removeAllObjects];
            for(NSDictionary *item in resultsArr){
                FollowingModel *following=[[FollowingModel alloc]init];
                following.userName=[item objectForKey:@"text"];
                [_followingArr addObject:following];
            }
            [_followingtableview reloadData];
            }else {
                _followingtableview.hidden=YES;
                _nodatalabel.text=@"لايوجد متابعين !";

            }
            [mProces removeFromSuperview];
                
        } failure:^(AFHTTPRequestOperation *operation,NSError *error){
            
            NSLog(@"Failed %@",error);
            [mProces removeFromSuperview];
            
        }];
        
        [operation start];
    }else {
        
    }
}
-(void) fetchFollowing{
    if([[AppManager sharedManager]accessToken]!=nil){
        NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
        NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetUserFollowings><info:AuthToken>%@</info:AuthToken></info:GetUserFollowings></soapenv:Body></soapenv:Envelope>",[[AppManager sharedManager]accessToken]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPMethod:@"POST"]; //GET
        
        [request addValue:@"http://info-est.com/GetUserFollowings" forHTTPHeaderField:@"SOAPAction"];
        
        [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:mProces];
        mProces.animationType=MBProgressHUDAnimationFade;
        mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
        [mProces show:YES];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
            
            NSLog(@"Success : Content : %@",responseObject);
            NSLog(@"Success : Content : %@",[operation responseString]);
            NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSError *error = nil;
            NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
            NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
            NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
            NSDictionary *dic3=[dic2 objectForKey:@"GetUserFollowingsResponse"];
            NSDictionary *following=[dic3 objectForKey:@"GetUserFollowingsResult"];
            NSDictionary *resultr=[following objectForKey:@"Result"];

            NSArray *resultsArr=[resultr objectForKey:@"string"];
            if(resultsArr !=nil &&resultsArr.count>0){
            [_followingArr removeAllObjects];
            for(NSDictionary *item in resultsArr){
                FollowingModel *following=[[FollowingModel alloc]init];
                following.userName=[item objectForKey:@"text"];
                [_followingArr addObject:following];
            }
            [_followingtableview reloadData];
            }else {
                
                
            }
            [mProces removeFromSuperview];
        } failure:^(AFHTTPRequestOperation *operation,NSError *error){
            
            NSLog(@"Failed %@",error);
            [mProces removeFromSuperview];
            
        }];
        
        [operation start];
    }else {
        
    }
}
/*
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_followingArr count]>1) {
        // If row is deleted, remove it from the list.
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            [tableView beginUpdates];
            [_followingArr removeObjectAtIndex:indexPath.row];
            
            // Animate the deletion from the table.
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            [tableView endUpdates];
        }
    }
}*/
-(void)removeCellFromTableView:(SWTableViewCell *)cell{
    NSIndexPath *cellIndexPath = [_followingtableview indexPathForCell:cell];
    
    [_followingArr removeObjectAtIndex:cellIndexPath.row];
    [_followingtableview deleteRowsAtIndexPaths:@[cellIndexPath]
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    if(_followingArr.count==0){
        _followingtableview.hidden=YES;
       
        _nodatalabel.text=@"انت لاتتابع احد الان !";
       
    }
    
    
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            if([[AppManager sharedManager]isFollowers]){
                [self removeCellFromTableView:cell];
            }
             
            else{
                [self removeFollowing:index];
                [self removeCellFromTableView:cell];
                

            
            }
            break;
        
        default:
            break;
    }
}
- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
      [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                 title:@"حذف متابعه"];
  
    
    return leftUtilityButtons;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _followingArr.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

-(void) removeFollowing:(NSInteger)index{
    if([[AppManager sharedManager]accessToken]!=nil){
        NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
        NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:UnfollowPublisher><info:PublisherUserName>%@</info:PublisherUserName><info:AuthToken>%@</info:AuthToken></info:UnfollowPublisher></soapenv:Body></soapenv:Envelope>",[[_followingArr objectAtIndex:index]userName],[[AppManager sharedManager]accessToken]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPMethod:@"POST"]; //GET
        
        [request addValue:@"http://info-est.com/UnfollowPublisher" forHTTPHeaderField:@"SOAPAction"];
        
        [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
  ///      MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
     //   [self.view addSubview:mProces];
//        mProces.animationType=MBProgressHUDAnimationFade;
  //      mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    //    [mProces show:YES];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
            
            NSLog(@"Success : Content : %@",responseObject);
            NSLog(@"Success : Content : %@",[operation responseString]);
            NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSError *error = nil;
            NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
            NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
            NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        //    [self fetchFollowing];
      //      [mProces removeFromSuperview];
        } failure:^(AFHTTPRequestOperation *operation,NSError *error){
            
            NSLog(@"Failed %@",error);
       //     [mProces removeFromSuperview];
            
        }];
        
        [operation start];
    }else {
        
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Add a shadow to the top view so it looks like it is on top of the others
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [[UIColor blackColor] CGColor];
    
    // Tell it which view should be created under left
    if (![self.slidingViewController.underRightViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    
    // Add the pan gesture to allow sliding
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    [self.navigationController.navigationBar setHidden:YES ];
}


- (IBAction)openmenuaction:(id)sender {
    if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered){
        [self.slidingViewController anchorTopViewToLeftAnimated:YES];
    }
    else{
        [self.slidingViewController resetTopViewAnimated:YES];
    }
    
}
- (IBAction)backbtnaction:(id)sender {
        self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeid"];
}
@end
