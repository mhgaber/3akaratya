//
//  Property.h
//  3akaratya
//
//  Created by mhGaber on 7/6/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Property : NSObject
@property (strong,nonatomic) NSString *propertyID;
@property(nonatomic) BOOL isClosed;
@property (strong,nonatomic) NSString *propertyTypeId;
@property(strong,nonatomic) NSString *propertyLocationId;
@property(strong,nonatomic) NSString *propertyName;
@property(nonatomic) double geoLat;
@property(nonatomic) double geoLng;
@property(nonatomic) double price;
@property(nonatomic) double maxBid;
@property(strong,nonatomic) NSString *userName;
@property(strong,nonatomic) NSString *publisherName;
@property(strong,nonatomic) NSString *requestType;
@property(strong,nonatomic) NSString *rentPriceDuration;
@property(nonatomic) int *rooms;
@property(nonatomic) double area;
@property(nonatomic) int floors;
@property(nonatomic) int views;
@property(nonatomic) int units;
@property(nonatomic) int bathrooms;
@property(strong,nonatomic) NSString *creationDate;
@property(nonatomic)BOOL isFavorited;
@property(nonatomic) BOOL isFollowed;
@property(strong,nonatomic) NSString *thumbURL;
@property(strong,nonatomic) NSString *publisherImageUrl;
@property(strong,nonatomic) NSString *locationName;


@end
