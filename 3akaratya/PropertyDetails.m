//
//  PropertyDetails.m
//  3akaratya
//
//  Created by mhGaber on 7/30/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "PropertyDetails.h"
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "XMLReader.h"
#import "PropertyDetailsModel.h"
#import "AgenData.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <AFNetworking/UIButton+AFNetworking.h>
#import "AppManager.h"
#import "ImagesGalleryyViewController.h"
#import <Social/Social.h>
#import "LocationModel.h"
@implementation PropertyDetails
- (void)viewWillAppear:(BOOL)animated
{
   
    [self.navigationController.navigationBar setHidden:NO ];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _locationsArrr=[[AppManager sharedManager]locationsArr];
    _propertyImagesGallery=[[NSMutableArray alloc]init];
self.navigationController.navigationBar.topItem.title = @"رجوع";
    _commentsonproperty.titleLabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:24];
    _foloowpublishertext.titleLabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:24];
    _askmoreinfo.titleLabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:24];
    _addfavbtn.titleLabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:24];
        _addresslabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:24];

    [_addresslabel sizeToFit];
    _desctitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:24];
    _siteonmaptitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:24];
    
    _datetitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    _roomstitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    _floorstitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    _yearstitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    _viewstitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    _unitstitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    _areatitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    _propertytitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:24];
    
    _pricelabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    
    _datelabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    
    _propertyDescription.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    
    _roomslabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    
    _unitslabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    
    _arealabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    
    _viewslabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    
    _floorslabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];
    _propertyIDLabel.font=[UIFont fontWithName:@"Tanseek M Medium" size:18];

    [self fetchData];
}
-(void)fetchData{
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetPropertyById><info:Id>%@</info:Id></info:GetPropertyById> </soapenv:Body></soapenv:Envelope>",_propertyID];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/GetPropertyById" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        NSDictionary *dic3=[dic2 objectForKey:@"GetPropertyByIdResponse"];
        NSDictionary *dic4=[dic3 objectForKey:@"GetPropertyByIdResult"];
        PropertyDetailsModel *propertyDetails=[[PropertyDetailsModel alloc]init];
        propertyDetails.propertyID=[[[dic4 objectForKey:@"ID"]objectForKey:@"text"] integerValue];
        propertyDetails.isClosed=(BOOL)[dic4 objectForKey:@"IsClosed"];
        propertyDetails.propertyTypeID=[[dic4 objectForKey:@"PropertyTypeId"]objectForKey:@"text"];
        propertyDetails.propertyLocationID=[[[dic4 objectForKey:@"PropertyLocationId"]objectForKey:@"text"] integerValue];
        propertyDetails.propertyName=[[dic4 objectForKey:@"PropertyName"]objectForKey:@"text"];
        propertyDetails.propertyDescription=[[dic4 objectForKey:@"PropertyDescription"]objectForKey:@"text"];
        propertyDetails.propertyNeighborhoodDescription=[[dic4 objectForKey:@"NeighborhoodDescription"]objectForKey:@"text"];
        propertyDetails.lat=[[[dic4 objectForKey:@"GeoLat"]objectForKey:@"text"] floatValue];
        propertyDetails.lon=[[[dic4 objectForKey:@"GeoLng"]objectForKey:@"text"] floatValue];
        propertyDetails.price=[[[dic4 objectForKey:@"Price"]objectForKey:@"text"] floatValue];
        propertyDetails.currency=[[dic4 objectForKey:@"Currency"]objectForKey:@"text"];
        propertyDetails.userName=[[dic4 objectForKey:@"UserName"]objectForKey:@"text"];
        propertyDetails.publisherName=[[dic4 objectForKey:@"PublisherName"]objectForKey:@"text"];
        propertyDetails.requesttype=[[dic4 objectForKey:@"RequestType"]objectForKey:@"text"];
        propertyDetails.rentPriceDuration=[[[dic4 objectForKey:@"RentPriceDuration"]objectForKey:@"text"] integerValue];
        propertyDetails.rooms=[[[dic4 objectForKey:@"Rooms"]objectForKey:@"text"] integerValue];
        propertyDetails.area=[[[dic4 objectForKey:@"Area"]objectForKey:@"text"] floatValue];
        propertyDetails.floors=[[[dic4 objectForKey:@"Floors"]objectForKey:@"text"] integerValue];
        propertyDetails.views=[[[dic4 objectForKey:@"Views"]objectForKey:@"text"] integerValue];
        propertyDetails.units=[[[dic4 objectForKey:@"Units"]objectForKey:@"text"] integerValue];
        propertyDetails.baths=[[[dic4 objectForKey:@"Bathrooms"]objectForKey:@"text"] integerValue];
        propertyDetails.creationDate=[[dic4 objectForKey:@"CreationDate"]objectForKey:@"text"];
        propertyDetails.isFavorited=(BOOL)[dic4 objectForKey:@"IsFavorited"];
        propertyDetails.isFollowed=(BOOL)[dic4 objectForKey:@"IsFollowed"];
        propertyDetails.thumbURL=[[dic4 objectForKey:@"ThumbUrl"]objectForKey:@"text"];
        propertyDetails.publisherThumb=[[dic4 objectForKey:@"PublisherImageUrl"]objectForKey:@"text"];
        NSDictionary *photos=[dic4 objectForKey:@"Photos"];
        if([[photos objectForKey:@"PropertyMediaItem"] isKindOfClass:[NSArray class] ]){
            NSArray *resultsList=[photos objectForKey:@"PropertyMediaItem"];
            propertyDetails.propertyPhotos=[[NSMutableArray alloc]init];
            
            for (NSDictionary *item in resultsList)
            {
                if(item.count>0)
                    [propertyDetails.propertyPhotos addObject:[[item objectForKey:@"Url"]objectForKey:@"text"]];
            }
        }
        NSDictionary *dic5=[dic3 objectForKey:@"AgentInfo"];
        propertyDetails.agentTitle=[[dic5 objectForKey:@"Title"] objectForKey:@"text"];
        propertyDetails.agentAdress=[[dic5 objectForKey:@"Address"] objectForKey:@"text"];
        propertyDetails.agentPhone=[[dic5 objectForKey:@"Phone"] objectForKey:@"text"];
        propertyDetails.agentWorkPhone=[[dic5 objectForKey:@"WorkPhone"] objectForKey:@"text"];
        propertyDetails.agentMobile=[[dic5 objectForKey:@"Mobile"] objectForKey:@"text"];
        propertyDetails.agentCountry=[[dic5 objectForKey:@"CountryISO3"] objectForKey:@"text"];
        propertyDetails.agentCity=[[dic5 objectForKey:@"City"] objectForKey:@"text"];
        for(int i=0;i<_locationsArrr.count;i++){
            LocationModel *locat=[_locationsArrr objectAtIndex:i];

            if([locat.location_id integerValue] ==propertyDetails.propertyLocationID){
                
                propertyDetails.locationName=locat.location_name;
               // NSLog([NSString stringWithFormat:@"%@"],locat.location_name);
                break;
            }
        }
        _propertyDetails=propertyDetails;

        [[AppManager sharedManager]setPropertyDetailsModel:_propertyDetails];
        [self initValues];
        [mProces removeFromSuperview];
        
   
        
        
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        //NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];
        [self fetchData];
    }];
    
    [operation start];
}
- (void) imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index{
//    [self.navigationController performSegueWithIdentifier:@"showgalleryimages" sender:self];
    //ImagesGalleryyViewController *controller = [[ImagesGalleryyViewController alloc] initWithNibName:@"ImagesGalleryyViewController" bundle:nil];
  //  [self presentViewController:controller animated:YES completion:nil];

//    [self performSegueWithIdentifier:@"showgalleryimages" sender:self];
}

-(void) initValues{
    if(_propertyDetails.price>0.0)
    _pricelabel.text=[NSString stringWithFormat:@"%.2f",_propertyDetails.price];
    else
            _pricelabel.text=@"غير محدد";
//    _pricelabel.text=[NSString stringWithFormat:@"%.2f",_propertyDetails.price];
    _roomslabel.text=[NSString stringWithFormat:@"%d",_propertyDetails.rooms];
    _datelabel.text=[_propertyDetails.creationDate substringToIndex:10];
    _propertyDescription.text=_propertyDetails.propertyDescription;
    
    [_propertyDescription sizeToFit];
        _unitslabel.text=[NSString stringWithFormat:@"%d",_propertyDetails.units];
    _arealabel.text=[NSString stringWithFormat:@"%.fم",_propertyDetails.area];
      _viewslabel.text=[NSString stringWithFormat:@"%d",_propertyDetails.views];
        _floorslabel.text=[NSString stringWithFormat:@"%d",_propertyDetails.floors];
    
    _addresslabel.text=[NSString stringWithFormat:@"%@",_propertyDetails.locationName];
    
    _propertyGallery.pageControl.currentPageIndicatorTintColor = [UIColor lightGrayColor];
    _propertyGallery.pageControl.pageIndicatorTintColor = [UIColor blackColor];
    _propertyGallery.slideshowTimeInterval = 5.5f;
    _propertyGallery.slideshowShouldCallScrollToDelegate = YES;
//    [_propertytitle sizeToFit];
    _propertytitle.numberOfLines=3;
    _propertytitle.text=_propertyDetails.propertyName;
    _propertyGallery.dataSource=self;
    _propertyGallery.delegate=self;
    [_propertyGallery reloadData];
    _propertyIDLabel.text=[NSString stringWithFormat:@"#%@",_propertyID];
    _yearlabel.text=[_propertyDetails.creationDate substringToIndex:4];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_propertyDetails.lat
                                                            longitude: _propertyDetails.lon
                                                                 zoom:6];
   // [_publisherthumb setImageForState:UIControlStateNormal  withURL:[[NSURL alloc]initWithString:_propertyDetails.publisherThumb]];
    [_publisherthumb setBackgroundImageForState:UIControlStateNormal withURL:[[NSURL alloc]initWithString:_propertyDetails.publisherThumb]];
    
 CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_propertyDetails.lat, _propertyDetails.lon);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    if(_propertyDetails.agentMobile ==nil)
        _propertyDetails.agentMobile=@"";
    if(_propertyDetails.agentAdress ==nil)
        _propertyDetails.agentAdress=@"";
        if(_propertyDetails.agentTitle ==nil)
        _propertyDetails.agentTitle=@"";
    
    NSString *actionSheetTitle =[NSString stringWithFormat:@"تواصل مع %@",_propertyDetails.agentTitle];
    
    
    NSString *other1 = [NSString stringWithFormat:@"جوال علي %@",_propertyDetails.agentMobile];
    NSString *other2 = [NSString stringWithFormat:@"عنوانه علي %@",_propertyDetails.agentAdress];
    
    NSString *cancelTitle = @"إلغاء";
    
    _actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    
    marker.map = _propertyLocation;
    _propertyLocation.camera=camera;
    

    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex ==0){

        NSString *phoneNumber = [@"tel://" stringByAppendingString:_propertyDetails.agentMobile];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }else if(buttonIndex ==1){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = _propertyDetails.agentAdress;
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"تنبيه" message:@"تم حفظ عنوان المعلن" delegate:nil cancelButtonTitle:@"موافقه" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - KIImagePager DataSource
- (NSArray *) arrayWithImages
{
    if(_propertyDetails .propertyPhotos!=nil)
    return _propertyDetails.propertyPhotos;
    else{
        if(_propertyDetails.thumbURL ==nil)
               _propertyDetails.thumbURL=@"";
        return @[
                  _propertyDetails.thumbURL
                  ];
    
    }
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image
{
    return UIViewContentModeScaleToFill;
}



- (IBAction)shareaction:(id)sender {
    NSString *textToShare = [NSString stringWithFormat:@"%@ علي موقع العقاراتيه ",_propertyDetails.propertyName];
    NSString *myWebsite=[NSString stringWithFormat:@"http://www.info-est.com/ar/properties/%@/%d",_propertyDetails.userName,_propertyDetails.propertyID];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];

    
    [self presentViewController:activityVC animated:YES completion:nil];
}
- (IBAction)showagentinfo:(id)sender {
 
    [_actionSheet showInView:self.view];
    
    
}
- (IBAction)showmoreinfo:(id)sender {
        [_actionSheet showInView:self.view];
}

- (IBAction)followpublisheraction:(id)sender {
    if([[AppManager sharedManager]accessToken]!=nil){
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:FollowPublisher><info:PublisherUserName>%@</info:PublisherUserName> <info:AuthToken>%@</info:AuthToken></info:FollowPublisher></soapenv:Body></soapenv:Envelope>",_propertyDetails.userName,[[AppManager sharedManager]accessToken]];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/FollowPublisher" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        _followpublishertext.titleLabel.text=@"تم متابعه المعلن";
        
        
        [mProces removeFromSuperview];
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];
        
    }];
    
    [operation start];
    }else {

    }
}

- (IBAction)addtofavaction:(id)sender {
    
    if([[AppManager sharedManager]accessToken]!=nil){
        NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
        NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:AddPropertyToFavorites> <info:PropertyID>%d</info:PropertyID><info:AuthToken>%@</info:AuthToken></info:AddPropertyToFavorites></soapenv:Body></soapenv:Envelope>",_propertyDetails.propertyID,[[AppManager sharedManager]accessToken]];
         NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPMethod:@"POST"]; //GET
        
        [request addValue:@"http://info-est.com/AddPropertyToFavorites" forHTTPHeaderField:@"SOAPAction"];
        
        [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:mProces];
        mProces.animationType=MBProgressHUDAnimationFade;
        mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
        [mProces show:YES];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
            
            NSLog(@"Success : Content : %@",responseObject);
            NSLog(@"Success : Content : %@",[operation responseString]);
            NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSError *error = nil;
            NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
            NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
            NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
            _followpublishertext.titleLabel.text=@"تم متابعه المعلن";
            
            
            [mProces removeFromSuperview];
        } failure:^(AFHTTPRequestOperation *operation,NSError *error){
            
            NSLog(@"Failed %@",error);
            [mProces removeFromSuperview];
            
        }];
        
        [operation start];
    }else {
        
    }

}
- (IBAction)facebookshare:(id)sender {
    NSString *facebookShareLink =[NSString stringWithFormat:@"http://www.info-est.com/ar/properties/%@/%@",_propertyDetails.userName,_propertyID];
    NSString *titleToShare = _propertyDetails.propertyName;
    
    Class composeViewControllerClass = [SLComposeViewController class];
    
    if(composeViewControllerClass == nil || ![composeViewControllerClass isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        NSString *facebookLink = [NSString stringWithFormat:@"http://www.facebook.com/sharer.php?u=%@&t=%@", facebookShareLink, titleToShare];
        facebookLink = [facebookLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:facebookLink]];
        
    } else {
        
        SLComposeViewController *composeViewController = [composeViewControllerClass composeViewControllerForServiceType:SLServiceTypeFacebook];
        [composeViewController setInitialText:titleToShare];
        [composeViewController addURL:[NSURL URLWithString:facebookShareLink]];
        
        [self presentModalViewController:composeViewController animated:YES];
    }
}

- (IBAction)tweetaction:(id)sender {
    NSString *twitterLink =[NSString stringWithFormat:@"http://www.info-est.com/ar/properties/%@/%@",_propertyDetails.userName,_propertyID];
    NSString *titleToShare = _propertyDetails.propertyName;
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:[NSString stringWithFormat:@"ألق نظره علي %@ علي موقع العقاراتيه %@",titleToShare,twitterLink]];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"هناك خطأ ما تأكد من الشبكه او من توافر تطبيق التويتر"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
@end
