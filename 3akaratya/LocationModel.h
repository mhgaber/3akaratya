//
//  LocationModel.h
//  3akaratya
//
//  Created by mhGaber on 9/17/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationModel : NSObject
@property(strong,nonatomic) NSString *location_id;
@property(strong,nonatomic) NSString *location_name;
@property(strong,nonatomic) NSString *locationparent_id;
@property(strong,nonatomic) NSString *location_currency;

@end
