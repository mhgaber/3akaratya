//
//  LoginViewController.m
//  3akaratya
//
//  Created by mhGaber on 8/8/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "LoginViewController.h"
#import "AppManager.h"
#import <AFNetworking/AFNetworking.h>
#import "XMLReader.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
@implementation LoginViewController

- (IBAction)cancellogin:(id)sender {
    
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)viewDidLoad{

}
- (IBAction)dismisskeyboard:(id)sender {
    [_username resignFirstResponder];
    [_password resignFirstResponder];
}

- (IBAction)login:(id)sender {
    if(_username.text.length>0&&_password.text.length>0){
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:Login><info:UserName>%@</info:UserName><info:Password>%@</info:Password></info:Login></soapenv:Body></soapenv:Envelope>",_username.text,_password.text];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/Login" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        NSDictionary *dic3=[dic2 objectForKey:@"LoginResponse"];
        NSDictionary *dic4=[dic3 objectForKey:@"LoginResult"];
        NSString *accessToken=(NSString*)[dic4 objectForKey:@"text"];
        if([accessToken isEqualToString:@"ERROR|Access Denied"]){
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"تنبيه" message:@"هناك خطأ بكلمه المرور او باسم المستخدم" delegate:nil cancelButtonTitle:@"موافقه" otherButtonTitles:nil, nil];
            [alert show];
        }else {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"نجاح" message:@"تم تسجيل الدخول بنجاح" delegate:nil cancelButtonTitle:@"موافقه" otherButtonTitles:nil, nil];
            [alert show];
            
        [[AppManager sharedManager]setIsLoggedIn:YES];
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isLoggedIn"];
            [[NSUserDefaults standardUserDefaults ]setObject:accessToken forKey:@"accessToken"];
            [[AppManager sharedManager]setAccessToken:accessToken];
            self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeid"];
            
            
        }
        //   _termstextview.text=text;
       
        
        [mProces removeFromSuperview];
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        //NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];

    }];
    
    [operation start];
    }else {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"تنبيه" message:@"لابد من ادخال كلمه السر واسم المستخدم" delegate:nil cancelButtonTitle:@"موافقه" otherButtonTitles:nil, nil];
        [alert show];
    }
   
}

@end
