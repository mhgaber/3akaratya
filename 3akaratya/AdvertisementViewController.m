//
//  AdvertisementViewController.m
//  3akaratya
//
//  Created by mhGaber on 8/3/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "AdvertisementViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "AdvertisingTableViewCell.h"
#import "AppManager.h"
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "AdvertisingModel.h"
#import "XMLReader.h"
@implementation AdvertisementViewController

-(void)viewDidLoad{
    _advertisementArr=[[NSMutableArray alloc]init];
    _advertisingtableview.delegate=self;
    _advertisingtableview.dataSource=self;

    [self fetchAdvertisement];
}
- (IBAction)showmenu:(id)sender {
    if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered){
        [self.slidingViewController anchorTopViewToLeftAnimated:YES];
    }
    else{
        [self.slidingViewController resetTopViewAnimated:YES];
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AdvertisingModel *adv=[_advertisementArr objectAtIndex:indexPath.row];
    if(adv.targetURL !=nil)
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: adv.targetURL]];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"advertisingCell";
    
    AdvertisingTableViewCell *cell = (AdvertisingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                                                     forIndexPath:indexPath];
 //   cell.leftUtilityButtons = [self leftButtons];
    AdvertisingModel *adv=[_advertisementArr objectAtIndex:indexPath.row];
    [cell.advtext sizeToFit];
    cell.advtext.text=[NSString stringWithFormat:@"• %@",adv.text];
    cell.delegate = self;
    
    return cell;
    
    
    
}
- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                               title:@"غير راغب في الإعلان"];
    
    
    return leftUtilityButtons;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _advertisementArr.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Add a shadow to the top view so it looks like it is on top of the others
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [[UIColor blackColor] CGColor];
    
    // Tell it which view should be created under left
    if (![self.slidingViewController.underRightViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    
    // Add the pan gesture to allow sliding
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    [self.navigationController.navigationBar setHidden:YES ];
}

-(void)fetchAdvertisement{
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetAdvertisements><info:SkipResults>0</info:SkipResults><info:TakeResults>10</info:TakeResults></info:GetAdvertisements></soapenv:Body></soapenv:Envelope>"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/GetAdvertisements" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    [self.view bringSubviewToFront:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        NSDictionary *dic3=[dic2 objectForKey:@"GetAdvertisementsResponse"];
        NSDictionary *following=[dic3 objectForKey:@"GetAdvertisementsResult"];
        NSDictionary *resultr=[following objectForKey:@"Result"];
        
        NSArray *resultsArr=[resultr objectForKey:@"MobileAdvertisement"];
        if(resultsArr !=nil &&resultsArr.count>0){
            [_advertisementArr removeAllObjects];
            for(NSDictionary *item in resultsArr){
                AdvertisingModel *adv=[[AdvertisingModel alloc]init];
//                adv.advID=[[[item objectForKey:@"Id"] objectForKey:@"text"] integerValue];
                adv.text=[[item objectForKey:@"Text"]objectForKey:@"text"];
                NSDictionary *targetURL=[item objectForKey:@"TargetUrl"];
                if([targetURL objectForKey:@"text"]!=nil)
                adv.targetURL=[targetURL objectForKey:@"text"];
                [_advertisementArr addObject:adv];
            }
            [_advertisingtableview reloadData];
        }else {
            
            
        }
        [mProces removeFromSuperview];
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];
        
    }];
    
    [operation start];
}

- (IBAction)backbtnaction:(id)sender {
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeid"];
   // [self.slidingViewController resetTopViewAnimated:YES];
}
@end
