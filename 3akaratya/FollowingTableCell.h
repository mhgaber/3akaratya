//
//  FollowingTableCell.h
//  3akaratya
//
//  Created by mhGaber on 8/3/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "SWTableViewCell.h"

@interface FollowingTableCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *tradename;

@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet UIImageView *thumb;

@end
