//
//  ComposeMessageViewController.m
//  3akaratya
//
//  Created by mhGaber on 9/16/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "ComposeMessageViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "AppManager.h"
#import "XMLReader.h"
@implementation ComposeMessageViewController
-(void)viewDidLoad{

}
- (IBAction)backbtnact:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sendbtnac:(id)sender {
    
    if(_messagebody.text.length>0&&_subjectmessagefield.text.length>0&&_sendtofield.text.length>0){
        NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
        
        NSString *soapBody=[NSString stringWithFormat:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:SendMailMessage><info:AuthToken>%@</info:AuthToken><info:Subject>%@</info:Subject><info:BodyText>%@</info:BodyText><info:ToUserNames>%@</info:ToUserNames></info:SendMailMessage></soapenv:Body></soapenv:Envelope>",
                             [[AppManager sharedManager]accessToken],_subjectmessagefield.text,_messagebody.text,_sendtofield.text ];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPMethod:@"POST"]; //GET
        
        [request addValue:@"http://info-est.com/SendMailMessage" forHTTPHeaderField:@"SOAPAction"];
        
        [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:mProces];
        mProces.animationType=MBProgressHUDAnimationFade;
        mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
        [mProces show:YES];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
            
            NSLog(@"Success : Content : %@",responseObject);
            NSLog(@"Success : Content : %@",[operation responseString]);
       
            [mProces removeFromSuperview];
        } failure:^(AFHTTPRequestOperation *operation,NSError *error){
            
            //NSLog(@"Failed %@",error);
            [mProces removeFromSuperview];
        }];
        
        [operation start];
    
    }else {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"تحذير" message:@"يجب ملئ جميع الحقول" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil];
        [alert show];
    }
}
/*

-(NSArray *)dataForPopoverInTextField:(MPGTextField *)textField{

    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *soapBody=[NSString stringWithFormat:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetUsers><info:UserNameOrFulllName>%@</info:UserNameOrFulllName><info:SkipResults>0</info:SkipResults><info:TakeResults>10</info:TakeResults></info:GetUsers></soapenv:Body></soapenv:Envelope>",_sendtofield.text] ;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/SendMailMessage" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        
        [mProces removeFromSuperview];
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        //NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];
    }];
    
    [operation start];
    
}
 */



@end
