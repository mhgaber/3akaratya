//
//  MyAccountViewController.h
//  3akaratya
//
//  Created by mhGaber on 8/21/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QBImagePickerController/QBImagePickerController.h>

@interface MyAccountViewController : UITableViewController<QBImagePickerControllerDelegate>
- (IBAction)chooseImagePicker:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *fullname;
@property (weak, nonatomic) IBOutlet UITextField *addressone;

@property (weak, nonatomic) IBOutlet UITextField *addresstwo;
@property (weak, nonatomic) IBOutlet UITextField *city;
@property (weak, nonatomic) IBOutlet UITextField *country;
@property (weak, nonatomic) IBOutlet UITextField *postalcode;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *workphone;
@property (weak, nonatomic) IBOutlet UIButton *savedatabutton;

- (IBAction)saveData:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *mobileno;
@property (weak, nonatomic) IBOutlet UITextField *faxno;

- (IBAction)backbtnaction:(id)sender;
@end
