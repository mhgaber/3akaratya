//
//  InboxViewController.m
//  3akaratya
//
//  Created by mhGaber on 8/2/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "InboxViewController.h"
#import "MessageTableViewCell.h"
#import "AppManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFNetworking.h>
#import "XMLReader.h"
#import "MessageModel.h"
@implementation InboxViewController
-(void)viewDidLoad{
    [super viewDidLoad];
//    self.navigationController.title=@"البريد(٢)";
    _messagesTableView.delegate=self;
    _messagesTableView.dataSource=self;
    _messagesArray=[[NSMutableArray alloc]init];
    [self fetchData:@"Inbox"];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _messagesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"messagerow";
    
    MessageTableViewCell *cell = [_messagesTableView
                      dequeueReusableCellWithIdentifier:simpleTableIdentifier
                      forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[MessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    MessageModel *message=[_messagesArray objectAtIndex:indexPath.row];
    
    if(!message.isOpened)
        cell.messagethumb.image=[UIImage imageNamed:@"unreadmessage"];
    cell.messagetitle.text=message.subject;
    cell.messagesender.text=message.senderUserName;
    cell.messagedate.text=message.date;
    return  cell;

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"messagedetails" sender:self];
}
- (IBAction)sendaction:(id)sender {
}

- (IBAction)receivedaction:(id)sender {
   

}
-(void)fetchData: (NSString*)flag{
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetMails><info:AuthToken>%@</info:AuthToken><info:Folder>%@</info:Folder><info:SkipResults>0</info:SkipResults><info:TakeResults>50</info:TakeResults></info:GetMails></soapenv:Body></soapenv:Envelope>",[[AppManager sharedManager]accessToken],flag];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/GetMails" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        NSDictionary *dic3=[dic2 objectForKey:@"GetMailsResponse"];
        NSDictionary *following=[dic3 objectForKey:@"GetMailsResult"];
        NSDictionary *resultr=[following objectForKey:@"Result"];
        
        NSArray *resultsArr=[resultr objectForKey:@"MailMessageHeader"];
        if(resultsArr !=nil &&resultsArr.count>0){
            [_messagesArray removeAllObjects];
            for(NSDictionary *item in resultsArr){
                MessageModel *model=[[MessageModel alloc]init];
                
                model.id_message=[[item objectForKey:@"Id"]objectForKey:@"text"];
                model.subject=[[item objectForKey:@"Subject"]objectForKey:@"text"];
                model.date=[[item objectForKey:@"Date"]objectForKey:@"text"];
                model.senderFullName=[[item objectForKey:@"SenderFullName"]objectForKey:@"text"];
                model.senderUserName=[[item objectForKey:@"SenderUserName"]objectForKey:@"text"];
                model.receiveFullName=[[item objectForKey:@"ReceiverFullName"]objectForKey:@"text"];
                model.receiveUserName=[[item objectForKey:@"ReceiverUserName"]objectForKey:@"text"];
                model.isOpened=[[item objectForKey:@"IsOpened"]objectForKey:@"text"];
                [_messagesArray addObject:model];
            }
            [[AppManager sharedManager]setMessages:_messagesArray];
            [_messagesTableView reloadData];
        }else {
            
            
        }
        [mProces removeFromSuperview];
        
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        //NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];
        
    }];
    
    [operation start];
}
- (IBAction)inboxaction:(id)sender {
    [_inboxbtn setBackgroundColor:[UIColor colorWithRed:100.0/255.0 green:13.0/255.0 blue:1.0/255.0 alpha:1.0]];
    [_sentbtn setBackgroundColor:[UIColor clearColor]];
    _inboxbtn.titleLabel.textColor=[UIColor whiteColor];
    _sentbtn.titleLabel.textColor=[UIColor blueColor];
    [self fetchData:@"Inbox"];
    
}

- (IBAction)sentaction:(id)sender {
       [_sentbtn setBackgroundColor:[UIColor colorWithRed:100.0/255.0 green:13.0/255.0 blue:1.0/255.0 alpha:1.0]];
    [_inboxbtn setBackgroundColor:[UIColor clearColor]];
    _sentbtn.titleLabel.textColor=[UIColor whiteColor];
    _inboxbtn.titleLabel.textColor=[UIColor blueColor];
        [self fetchData:@"Sent"];
}
@end
