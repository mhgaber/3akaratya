//
//  MapViewController.m
//  3akaratya
//
//  Created by mhGaber on 7/13/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "MapViewController.h"

#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManager.h>
#import <GoogleMaps/GoogleMaps.h>
#import "MBProgressHUD.h"
#import <AFNetworking/AFNetworking.h>
#import "XMLReader.h"
#import "Property.h"
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@implementation MapViewController

- (void)viewDidLoad {
 
   
    _mapproperties.myLocationEnabled = YES;

    _locationManager = [[CLLocationManager alloc] init];
    _propertiesLista=[[NSMutableArray alloc]init];
    _locationManager.delegate=self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
  /*
    if(IS_OS_8_OR_LATER) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
*/
            [self fetchData];
    

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;

    if (currentLocation != nil) {
      

       [_locationManager stopUpdatingLocation];
        
    }

}


- (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(void)fetchData{
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
   // NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetProperties><info:GeoLatFrom>%f</info:GeoLatFrom><info:GeoLngFrom>%f</info:GeoLngFrom><info:SkipResults>0</info:SkipResults><info:TakeResults>10</info:TakeResults><info:LoadFullDetails>false</info:LoadFullDetails></info:GetProperties></soapenv:Body></soapenv:Envelope>",_lat,_lon];
    
    NSString *soapBody=[NSString stringWithFormat:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetProperties><info:GeoLatFrom>26.4148241198690000 </info:GeoLatFrom><info:GeoLngFrom> 43.9135980606079100</info:GeoLngFrom><info:SkipResults>0</info:SkipResults><info:TakeResults>10</info:TakeResults><info:LoadFullDetails>false</info:LoadFullDetails></info:GetProperties></soapenv:Body></soapenv:Envelope>"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/GetProperties" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        NSDictionary *dic3=[dic2 objectForKey:@"GetPropertiesResponse"];
        NSDictionary *dic4=[dic3 objectForKey:@"GetPropertiesResult"];
        NSArray *resultsList=[dic4 objectForKey:@"PropertyItem"];
        for (NSDictionary *item in resultsList)
        {
            Property *property=[[Property alloc]init];
            property.propertyID=[[item objectForKey:@"Id"] objectForKey:@"text"];
            property.isClosed=(BOOL)[item objectForKey:@"IsClosed"];
            property.propertyTypeId=[[item objectForKey:@"PropertyTypeId"] objectForKey:@"text"];
            property.propertyLocationId=[[item objectForKey:@"PropertyLocationId"] objectForKey:@"text"];
            property.propertyName=[[item objectForKey:@"PropertyName"] objectForKey:@"text"];
            
            property.geoLat=[[[item objectForKey:@"GeoLat"] objectForKey:@"text"] doubleValue];
            property.geoLng=[[[item objectForKey:@"GeoLat"] objectForKey:@"text"] doubleValue];
            property.price=[[[item objectForKey:@"Price"] objectForKey:@"text"] doubleValue];
            property.maxBid=[[[item objectForKey:@"MaxBid"] objectForKey:@"text"] doubleValue];
            property.userName=[[item objectForKey:@"UserName"] objectForKey:@"text"];
            property.publisherName=[[item objectForKey:@"PublisherName"] objectForKey:@"text"];
            property.requestType=[[item objectForKey:@"RequestType"] objectForKey:@"text"];
            property.rentPriceDuration=[[item objectForKey:@"RentPriceDuration"] objectForKey:@"text"];
            property.rooms=[[[item objectForKey:@"Rooms"] objectForKey:@"text"]integerValue];
            property.area=[[[item objectForKey:@"Area"] objectForKey:@"text"] doubleValue];
            property.floors=[[[item objectForKey:@"Floors"] objectForKey:@"text"] integerValue];
            property.views=[[[item objectForKey:@"Views"] objectForKey:@"text"] integerValue];
            property.units=[[[item objectForKey:@"Units"] objectForKey:@"text"] integerValue];
            property.bathrooms=[[[item objectForKey:@"Bathrooms"] objectForKey:@"text"] integerValue];
            property.creationDate=[[item objectForKey:@"CreationDate"] objectForKey:@"text"];
            property.isFavorited=(BOOL)[item objectForKey:@"IsFavorited"];
            property.isFollowed=(BOOL)[item objectForKey:@"IsFollowed"];
            property.thumbURL=[[item objectForKey:@"ThumbUrl"] objectForKey:@"text"];
            property.publisherImageUrl=[[item objectForKey:@"PublisherImageUrl"] objectForKey:@"text"];
            [_propertiesLista addObject:property];
        }
        [self initMapView];
        [mProces removeFromSuperview];
        

        
        
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];
        [self fetchData];
    }];
    
    [operation start];
}
- (IBAction)returntogrid:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void) initMapView{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:26.4148241198690000
                                                            longitude:43.9135980606079100
                                                                 zoom:10];
    _mapproperties.camera=camera;
    for(int i=0;i<_propertiesLista.count;i++){
        Property *property=[_propertiesLista objectAtIndex:i];
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(property.geoLat,property.geoLng);
    GMSMarker *london = [GMSMarker markerWithPosition:position];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,60,60)];
    UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mapmarker"]];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(12, 15, 30, 20)];
    label.text = [NSString stringWithFormat:@"%.2f",property.price];
        
    label.textColor=[UIColor whiteColor];
    label.textAlignment=UITextAlignmentCenter;
    [label sizeToFit];
    
    [view addSubview:pinImageView];
    [view addSubview:label];
    UIImage *markerIcon = [self imageFromView:view];
    london.icon=markerIcon;
    //  london.icon = [GMSMarker markerImageWithColor:[UIColor clearColor]];
    london.snippet=[NSString stringWithFormat:@"%.2f",property.price];
    
    london.map = _mapproperties;
    
    }
}
@end
