//
//  ImagesGalleryyViewController.m
//  3akaratya
//
//  Created by mhGaber on 9/25/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "ImagesGalleryyViewController.h"
#import "AppManager.h"
@interface ImagesGalleryyViewController ()

@end

@implementation ImagesGalleryyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _propertyDetailsModel=[[AppManager sharedManager]propertyDetailsModel];
    
    _imagesPaggers.pageControl.currentPageIndicatorTintColor = [UIColor lightGrayColor];
    _imagesPaggers.pageControl.pageIndicatorTintColor = [UIColor blackColor];
    _imagesPaggers.slideshowTimeInterval = 5.5f;
    _imagesPaggers.slideshowShouldCallScrollToDelegate = YES;
    //    [_propertytitle sizeToFit];

    _imagesPaggers.dataSource=self;
    _imagesPaggers.delegate=self;
    for(int i=0;i<_propertyDetailsModel.propertyPhotos.count;i++){
        NSLog([NSString stringWithFormat:@"%@",[_propertyDetailsModel.propertyPhotos objectAtIndex:i]]);
    }
    [_imagesPaggers reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - KIImagePager DataSource
- (NSArray *) arrayWithImages
{
    if(_propertyDetailsModel .propertyPhotos!=nil)
        return _propertyDetailsModel.propertyPhotos;
    else{
        if(_propertyDetailsModel.thumbURL ==nil)
            _propertyDetailsModel.thumbURL=@"";
        return @[
                 _propertyDetailsModel.thumbURL
                 ];
        
    }
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image
{
    return UIViewContentModeScaleToFill;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
