//
//  LoginCustomViewController.m
//  3akaratya
//
//  Created by mhGaber on 8/21/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "LoginCustomViewController.h"

@interface LoginCustomViewController ()

@end

@implementation LoginCustomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    self.view.opaque = YES;
    CALayer * l = [_backgroundview layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    // You can even add a border
    [l setBorderWidth:0.5];
    [l setBorderColor:[[UIColor redColor] CGColor]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closebutton:(id)sender {
    [self removeFromParentViewController];
}
@end
