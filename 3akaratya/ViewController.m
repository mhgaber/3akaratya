//
//  ViewController.m
//  3akaratya
//
//  Created by mhGaber on 7/5/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "ViewController.h"
#import "BigImageCell.h"
#import <AFNetworking/AFNetworking.h>
#import "XMLReader.h"
#import "Property.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "ThumbRow.h"
#import "MBProgressHUD.h"
#import "PropertyDetails.h"
#import "LoginViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "AppManager.h"
#import <QuartzCore/QuartzCore.h>
#import "CircularImageView.h"
#import "LocationModel.h"
@interface ViewController ()

@end

@implementation ViewController
BOOL isThumb;
int locationIndex=0;
- (void)viewDidLoad {
    [super viewDidLoad];
    _lat=0;
    _lon=0;
    _propertiesTable.delegate=self;
    _propertiesTable.dataSource=self;
    _requestsTypesArr=[[NSMutableArray alloc]init];
    _locationsdefined=[[NSMutableArray alloc]init];
    _propertiesList=[[NSMutableArray alloc]init];
    _locationsArr=[[NSMutableArray alloc]init];
    _locationsIDs=[[NSMutableArray alloc]init];
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate=self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if([[AppManager sharedManager]homeList]==nil){
        [self getLocations];
       
       
        [self fetRequests];
        [self fetchLocations];
    }
    else{
        _propertiesList=[[AppManager sharedManager]homeList];
        _requestsTypesArr=[[AppManager sharedManager]requestsArr];
        _locationsIDs=[[AppManager sharedManager]locationsIDs];
        _locations.isOptionalDropDown = NO;
        _locationsdefined=[[AppManager sharedManager] locationsDefined];
        _type.isOptionalDropDown = NO;
        [_type setItemList:_requestsTypesArr];
        [_propertiesTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        MenuViewController *menu=(MenuViewController*)[self.slidingViewController underRightViewController];

        [        [menu menutable] reloadData];
    }
    
    _logorregview.layer.borderColor = [UIColor colorWithRed:163.0/255.0 green:24.0/255.0 blue:31.0/255.0 alpha:1.0].CGColor;
    _logorregview.layer.borderWidth = 3.0f;
    _passwordtext.layer.borderColor = [UIColor colorWithRed:163.0/255.0 green:24.0/255.0 blue:31.0/255.0 alpha:1.0].CGColor;
    _passwordtext.layer.borderWidth = 3.0f;
    _emailtext.layer.borderColor = [UIColor colorWithRed:163.0/255.0 green:24.0/255.0 blue:31.0/255.0 alpha:1.0].CGColor;
    _emailtext.layer.borderWidth = 3.0f;
    _usernametext.layer.borderColor = [UIColor colorWithRed:163.0/255.0 green:24.0/255.0 blue:31.0/255.0 alpha:1.0].CGColor;
    _usernametext.layer.borderWidth = 3.0f;
    
    _logorregview.layer.cornerRadius = 8;
    _logorregview.layer.masksToBounds = YES;
    
    _usernametext.layer.cornerRadius = 3;
    _usernametext.layer.masksToBounds = YES;
    
    _passwordtext.layer.cornerRadius = 3;
    _passwordtext.layer.masksToBounds = YES;
    
    _emailtext.layer.cornerRadius = 3;
    _emailtext.layer.masksToBounds = YES;
    
    _regorloginbtn.layer.cornerRadius = 3;
    _regorloginbtn.layer.masksToBounds = YES;
    _filterView.layer.borderColor = [UIColor colorWithRed:163.0/255.0 green:24.0/255.0 blue:31.0/255.0 alpha:1.0].CGColor;
    _filterView.layer.borderWidth = 3.0f;
  
    
    _rentduration.isOptionalDropDown = NO;
    [_rentduration setItemList:[NSArray arrayWithObjects:@"شهر او اقل",@"٣ شهور",@"٦ شهور",@"سنه",@"٥ سنوات",@"اكثر من ٥ سنوات", nil]];

    _locations.layer.cornerRadius = 3;
    _locations.layer.masksToBounds = YES;
    _locations.layer.borderColor = [UIColor colorWithRed:163.0/255.0 green:24.0/255.0 blue:31.0/255.0 alpha:1.0].CGColor;
    _locations.layer.borderWidth = 3.0f;
    _type.layer.cornerRadius = 3;
    _type.layer.masksToBounds = YES;
    _type.layer.borderColor = [UIColor colorWithRed:163.0/255.0 green:24.0/255.0 blue:31.0/255.0 alpha:1.0].CGColor;
    _type.layer.borderWidth = 3.0f;
    _fromprice.layer.cornerRadius = 3;
    _fromprice.layer.masksToBounds = YES;
    _fromprice.layer.borderColor = [UIColor colorWithRed:163.0/255.0 green:24.0/255.0 blue:31.0/255.0 alpha:1.0].CGColor;
    _fromprice.layer.borderWidth = 3.0f;
    _toprice.layer.cornerRadius = 3;
    _toprice.layer.masksToBounds = YES;
    _toprice.layer.borderColor = [UIColor colorWithRed:163.0/255.0 green:24.0/255.0 blue:31.0/255.0 alpha:1.0].CGColor;
    _toprice.layer.borderWidth = 3.0f;
    _keywordfield.layer.cornerRadius = 3;
    _keywordfield.layer.masksToBounds = YES;
    _keywordfield.layer.borderColor = [UIColor colorWithRed:163.0/255.0 green:24.0/255.0 blue:31.0/255.0 alpha:1.0].CGColor;
    _keywordfield.layer.borderWidth = 3.0f;
    _rentduration.layer.cornerRadius = 3;
    _rentduration.layer.masksToBounds = YES;
    _rentduration.layer.borderColor = [UIColor colorWithRed:163.0/255.0 green:24.0/255.0 blue:31.0/255.0 alpha:1.0].CGColor;
    _rentduration.layer.borderWidth = 3.0f;

    
}

-(void)fetRequests{
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetDefinedPropertyTypes/></soapenv:Body></soapenv:Envelope>"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/GetDefinedPropertyTypes" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        NSDictionary *dic3=[dic2 objectForKey:@"GetDefinedPropertyTypesResponse"];
        NSDictionary *dic4=[dic3 objectForKey:@"GetDefinedPropertyTypesResult"];

        NSArray *resultsList=[dic4 objectForKey:@"KeyValuePairOfStringString"];
        for (NSDictionary *item in resultsList)
        {
            [_requestsTypesArr addObject:[[item objectForKey:@"Value"]objectForKey:@"text"]];
        }
        [mProces removeFromSuperview];
        [[AppManager sharedManager]setRequestsArr:_requestsTypesArr];
        _type.isOptionalDropDown = NO;
        [_type setItemList:_requestsTypesArr];
        //  }
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        //NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];
       // [self fetchData];
    }];
    
    [operation start];

}


-(void)fetchLocations{
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetDefinedLocations/></soapenv:Body></soapenv:Envelope>"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/GetDefinedLocations" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        NSDictionary *dic3=[dic2 objectForKey:@"GetDefinedLocationsResponse"];
        NSDictionary *dic4=[dic3 objectForKey:@"GetDefinedLocationsResult"];
        
        NSArray *resultsList=[dic4 objectForKey:@"PropertyLocationItem"];
        for (NSDictionary *item in resultsList)
        {
            [_locationsdefined addObject:[[item objectForKey:@"Name"]objectForKey:@"text"]];
            [_locationsIDs addObject:[[item objectForKey:@"Id"]objectForKey:@"text"]];
        }
        [mProces removeFromSuperview];
        [[AppManager sharedManager]setRequestsArr:_locationsdefined];
        
        _locations.isOptionalDropDown = NO;
        [_locations setItemList:_locationsdefined];
        

        //  }
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        //NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];
     //   [self fetchData];
    }];
    
    [operation start];
    
}

-(void)fetchData{
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetProperties><GeoLatFrom>%f</GeoLatFrom><GeoLngFrom>%f</GeoLngFrom><info:SkipResults>0</info:SkipResults><info:TakeResults>10</info:TakeResults><info:LoadFullDetails>false</info:LoadFullDetails></info:GetProperties></soapenv:Body></soapenv:Envelope>",_lat,_lon];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/GetProperties" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        NSDictionary *dic3=[dic2 objectForKey:@"GetPropertiesResponse"];
        NSDictionary *dic4=[dic3 objectForKey:@"GetPropertiesResult"];
        NSArray *resultsList=[dic4 objectForKey:@"PropertyItem"];
        for (NSDictionary *item in resultsList)
        {
            Property *property=[[Property alloc]init];
            property.propertyID=[[item objectForKey:@"Id"] objectForKey:@"text"];
            property.isClosed=(BOOL)[item objectForKey:@"IsClosed"];
            property.propertyTypeId=[[item objectForKey:@"PropertyTypeId"] objectForKey:@"text"];
            property.propertyLocationId=[[item objectForKey:@"PropertyLocationId"] objectForKey:@"text"];
            property.propertyName=[[item objectForKey:@"PropertyName"] objectForKey:@"text"];
            
            property.geoLat=[[[item objectForKey:@"GeoLat"] objectForKey:@"text"] doubleValue];
            property.geoLng=[[[item objectForKey:@"GeoLat"] objectForKey:@"text"] doubleValue];
            property.price=[[[item objectForKey:@"Price"] objectForKey:@"text"] doubleValue];
            property.maxBid=[[[item objectForKey:@"MaxBid"] objectForKey:@"text"] doubleValue];
            property.userName=[[item objectForKey:@"UserName"] objectForKey:@"text"];
            property.publisherName=[[item objectForKey:@"PublisherName"] objectForKey:@"text"];
            property.requestType=[[item objectForKey:@"RequestType"] objectForKey:@"text"];
            property.rentPriceDuration=[[item objectForKey:@"RentPriceDuration"] objectForKey:@"text"];
            property.rooms=[[[item objectForKey:@"Rooms"] objectForKey:@"text"]integerValue];
            property.area=[[[item objectForKey:@"Area"] objectForKey:@"text"] doubleValue];
            property.floors=[[[item objectForKey:@"Floors"] objectForKey:@"text"] integerValue];
            property.views=[[[item objectForKey:@"Views"] objectForKey:@"text"] integerValue];
            property.units=[[[item objectForKey:@"Units"] objectForKey:@"text"] integerValue];
            property.bathrooms=[[[item objectForKey:@"Bathrooms"] objectForKey:@"text"] integerValue];
            property.creationDate=[[item objectForKey:@"CreationDate"] objectForKey:@"text"];
            property.isFavorited=(BOOL)[item objectForKey:@"IsFavorited"];
            property.isFollowed=(BOOL)[item objectForKey:@"IsFollowed"];
            property.thumbURL=[[item objectForKey:@"ThumbUrl"] objectForKey:@"text"];
            property.publisherImageUrl=[[item objectForKey:@"PublisherImageUrl"] objectForKey:@"text"];
            [_propertiesList addObject:property];
        }
        [mProces removeFromSuperview];

        [_propertiesTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        
    //  [self performSegueWithIdentifier:@"showlogin" sender:self];
        [[AppManager sharedManager]setHomeList:_propertiesList];
    if(![[AppManager sharedManager]isFirstTime]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
      //  LoginViewController *privacy = (LoginViewController*)[storyboard instantiateViewControllerWithIdentifier:@"logincontroller"];

  //     [self.navigationController presentViewController:privacy animated:YES completion:nil];
        _logorregview.hidden=NO;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

            
            [defaults setBool:![[AppManager sharedManager]isFirstTime] forKey:@"isFirstTime"];
     }
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        NSLog(@"Failed %@",error);

        [mProces removeFromSuperview];
     //   [self fetchData];
    }];
    
    [operation start];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Add a shadow to the top view so it looks like it is on top of the others
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [[UIColor blackColor] CGColor];
    
    
    if (![self.slidingViewController.underRightViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    
    // Add the pan gesture to allow sliding
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    [self.navigationController.navigationBar setHidden:YES ];
    
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _propertiesList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Property *property=(Property*)[_propertiesList objectAtIndex:indexPath.row];
    if(property.thumbURL ==nil){
        static NSString *simpleTableIdentifier = @"thumbrow";
        
        ThumbRow *cell = [_propertiesTable
                              dequeueReusableCellWithIdentifier:simpleTableIdentifier
                              forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[ThumbRow alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        cell.propertyID.text=[NSString stringWithFormat:@"#%@",property.propertyID];
        cell.propertyRooms.text=[NSString stringWithFormat:@"%d",property.rooms];
        cell.propertyUnits.text=[NSString stringWithFormat:@"%d",property.units];
        cell.propertyArea.text=[NSString stringWithFormat:@"%.fم",property.area];
        cell.propertyViews.text=[NSString stringWithFormat:@"%d",property.views];

        if(property.price>0.0)
        cell.propertyPrice.text=[NSString stringWithFormat:@"%.f",property.price];
else
    cell.propertyPrice.text=@"غير محدد";
                cell.propertytitlethumb.text=property.propertyName;
                isThumb=NO;
        cell.propertyDate.text=[property.creationDate substringToIndex:10];
        cell.propertyDate.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyPrice.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyViews.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyArea.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyUnits.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyRooms.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyID.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertytitlethumb.font=[UIFont fontWithName:@"Tanseek M Medium" size:22];
        
                cell.datetitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
                cell.roomstitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
                cell.viewstitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
                cell.unitstitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
                cell.areatitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        if(property.publisherImageUrl!=nil){
            [cell.publisherimage setImageWithURL:[[NSURL alloc]initWithString:property.publisherImageUrl]];
            NSLog([NSString stringWithFormat:@"URL: %@, id=%d",property.publisherImageUrl, indexPath.row]);
        }
        if(indexPath.row%2!=0)
            cell.contentView.backgroundColor=[UIColor colorWithRed:225.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1.0 ];
        return cell;
        
    }else {
    static NSString *simpleTableIdentifier = @"bigImage";
    
    BigImageCell *cell = [_propertiesTable
                        dequeueReusableCellWithIdentifier:simpleTableIdentifier
                        forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[BigImageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
        isThumb=YES;
        cell.propertyID.text=[NSString stringWithFormat:@"#%@",property.propertyID];
        cell.propertyRooms.text=[NSString stringWithFormat:@"%d",property.rooms];
        cell.propertyUnits.text=[NSString stringWithFormat:@"%d",property.units];
        cell.propertyArea.text=[NSString stringWithFormat:@"%.fم",property.area];
        cell.propertyViews.text=[NSString stringWithFormat:@"%d",property.views];
        if(property.price>0.0)
            cell.propertyPrice.text=[NSString stringWithFormat:@"%.f",property.price];
        else
            cell.propertyPrice.text=@"غير محدد";
        
        
        
        cell.propertyDate.text=[property.creationDate substringToIndex:10];
        cell.propertyTitle.text=property.propertyName;
        [cell.propertyImage setImageWithURL:[[NSURL alloc]initWithString:property.thumbURL]];
       // [cell.publisherThumb setImageWithURL:[[NSURL alloc]initWithString:property.publisherImageUrl]];

        [cell.publisherThumb setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:property.publisherImageUrl]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            cell.publisherThumb.image = image;
            cell.publisherThumb.layer.cornerRadius = cell.publisherThumb.frame.size.width / 2;
            
            cell.publisherThumb.layer.masksToBounds = YES;
            cell.publisherThumb.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0].CGColor;
            cell.publisherThumb.layer.borderWidth = 2.5f;
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            NSLog(@"Request failed with error: %@", error);
        }];
        for(int i=0;i<_locationsArr.count;i++){
            LocationModel *locat=[_locationsArr objectAtIndex:i];
            NSString *locat_id=locat.location_id;
            if([locat_id isEqualToString:property.propertyLocationId]){
                cell.propertyLocation.text=locat.location_name;
                property.locationName=locat.location_name;
                NSLog([NSString stringWithFormat:@"%@"],locat.location_name);
                break;
            }
        }
        cell.propertyLocation.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyDate.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyID.font=[UIFont fontWithName:@"Tanseek M Medium" size:20];
        cell.propertyRooms.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyUnits.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyArea.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyViews.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyPrice.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.propertyTitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:22];
        cell.datetitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.roomstitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.viewstitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.unitstitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        cell.areatitle.font=[UIFont fontWithName:@"Tanseek M Medium" size:15];
        if(indexPath.row%2!=0)
            cell.contentView.backgroundColor=[UIColor colorWithRed:225.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1.0 ];
            return cell;
    }

}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isThumb) {
        return 110;
    }
    else {
        return 270;
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender

{
    if([segue.identifier isEqualToString:@"detailsfromlist"]){
        
        PropertyDetails *details=(PropertyDetails*)segue.destinationViewController;
        details.propertyID=_selectedProperty.propertyID;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectedProperty=(Property*)[_propertiesList objectAtIndex:indexPath.row];

    [self performSegueWithIdentifier:@"detailsfromlist" sender:self];
}


- (IBAction)openMenu:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        _filterView.hidden=NO;
        _filterView.alpha = 0;
    } completion: ^(BOOL finished) {
        
        _filterView.alpha=1;
    }];
    
    

}
-(void) getLocations{
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetDefinedLocations/></soapenv:Body></soapenv:Envelope>"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/GetDefinedLocations" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        NSDictionary *dic3=[dic2 objectForKey:@"GetDefinedLocationsResponse"];
        NSDictionary *dic4=[dic3 objectForKey:@"GetDefinedLocationsResult"];
        NSArray *resultsList=[dic4 objectForKey:@"PropertyLocationItem"];
        for (NSDictionary *item in resultsList)
        {
            LocationModel *location=[[LocationModel alloc]init];
            location.location_id=[[item objectForKey:@"Id"]objectForKey:@"text"];
            location.locationparent_id=[[item objectForKey:@"ParentId"]objectForKey:@"text"];
            location.location_name=[[item objectForKey:@"Name"]objectForKey:@"text"];
            location.location_currency=[[item objectForKey:@"Currency"]objectForKey:@"text"];

            [_locationsArr addObject:location];
        }
        
        [mProces removeFromSuperview];
        [        [AppManager sharedManager] setLocationsArr:_locationsArr];
         [self fetchData];

    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];

    }];
    
    [operation start];
    
}
- (IBAction)regorloginbtn:(id)sender {
    if(_usernametext.text.length>0&&_emailtext.text.length>0&&_passwordtext.text.length>0){
        NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
        NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:RegisterNewAccount><info:UserName>%@</info:UserName><info:Password>%@</info:Password><info:Email>%@</info:Email></info:RegisterNewAccount></soapenv:Body></soapenv:Envelope>",_usernametext.text,_passwordtext.text,_emailtext.text];
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPMethod:@"POST"]; //GET
        
        [request addValue:@"http://info-est.com/RegisterNewAccount" forHTTPHeaderField:@"SOAPAction"];
        
        [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:mProces];
        mProces.animationType=MBProgressHUDAnimationFade;
        mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
        [mProces show:YES];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
            
            NSLog(@"Success : Content : %@",responseObject);
            NSLog(@"Success : Content : %@",[operation responseString]);
            NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSError *error = nil;
            NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
            NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
            NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
            NSDictionary *dic3=[dic2 objectForKey:@"RegisterNewAccountResponse"];
            NSDictionary *dic4=[dic3 objectForKey:@"RegisterNewAccountResult"];
            NSString *result=[dic4 objectForKey:@"text"];
            if([result isEqualToString:@"SUCCESS"]){
                [self loginMethod];
            }else {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"تحذير" message:@"خطأ ف الاتصال" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil];
            }
            [mProces removeFromSuperview];
            
        } failure:^(AFHTTPRequestOperation *operation,NSError *error){
            
            //NSLog(@"Failed %@",error);
            [mProces removeFromSuperview];

        }];
        
        [operation start];
    }else {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"تحذير" message:@"لابد من ملئ جميع الحقول" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)loginMethod{
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:Login><info:UserName>%@</info:UserName><info:Password>%@</info:Password></info:Login></soapenv:Body></soapenv:Envelope>",_usernametext.text,_passwordtext.text];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/Login" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        NSDictionary *dic3=[dic2 objectForKey:@"LoginResponse"];
        NSDictionary *dic4=[dic3 objectForKey:@"LoginResult"];
        NSString *accessToken=(NSString*)[dic4 objectForKey:@"text"];
        [[AppManager sharedManager]setIsLoggedIn:YES];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isLoggedIn"];
        [[NSUserDefaults standardUserDefaults ]setObject:accessToken forKey:@"accessToken"];
        [[AppManager sharedManager]setAccessToken:accessToken];

        [mProces removeFromSuperview];
        
        
        [UIView animateWithDuration:0.3 animations:^{
            _logorregview.alpha = 0;
        } completion: ^(BOOL finished) {
            
            _logorregview.hidden = finished;
        }];
        
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        //NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];
        
    }];
    
    [operation start];
}

- (IBAction)closeaction:(id)sender {
    

    
}
-(void)showLogger{
    _logorregview.alpha = 0;
    _logorregview.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        _logorregview.alpha = 1;
    }];
}
- (IBAction)searchproperties:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
    NSString *locationID;
    for(int i=0;i<_locationsdefined.count;i++){
        NSString *name=[_locationsdefined objectAtIndex:i];
        if([name isEqualToString:_locations.text]){
            locationID=[_locationsIDs objectAtIndex:i];
            break;
        }
    }
    
    NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:GetProperties><info:AuthToken>%@</info:AuthToken><info:ByRequestType>%@</info:ByRequestType><info:ByPropertyType>%@</info:ByPropertyType><info:ByLocationId>%@</info:ByLocationId><info:ByMinPrice>%@</info:ByMinPrice><info:ByMaxPrice>%@</info:ByMaxPrice><info:ByKeyword>%@</info:ByKeyword><info:ByRentDuration>%@</info:ByRentDuration><info:SkipResults>0</info:SkipResults><info:TakeResults>10</info:TakeResults><info:LoadFullDetails>false</info:LoadFullDetails></info:GetProperties></soapenv:Body></soapenv:Envelope>",[[AppManager sharedManager]accessToken],@"For Sale",_type.text,locationID,_fromprice.text,_toprice.text,_keywordfield.text,@"10"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"]; //GET
    
    [request addValue:@"http://info-est.com/GetProperties" forHTTPHeaderField:@"SOAPAction"];
    
    [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:mProces];
    mProces.animationType=MBProgressHUDAnimationFade;
    mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
    [mProces show:YES];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        
        NSLog(@"Success : Content : %@",responseObject);
        NSLog(@"Success : Content : %@",[operation responseString]);
        NSData *xmlData = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData error:&error];
        NSDictionary *dic=[dict objectForKey:@"soap:Envelope"];
        NSDictionary *dic2=[dic objectForKey:@"soap:Body"];
        NSDictionary *dic3=[dic2 objectForKey:@"GetPropertiesResponse"];
        NSDictionary *dic4=[dic3 objectForKey:@"GetPropertiesResult"];
        NSArray *resultsList=[dic4 objectForKey:@"PropertyItem"];
        for (NSDictionary *item in resultsList)
        {
            Property *property=[[Property alloc]init];
            property.propertyID=[[item objectForKey:@"Id"] objectForKey:@"text"];
            property.isClosed=(BOOL)[item objectForKey:@"IsClosed"];
            property.propertyTypeId=[[item objectForKey:@"PropertyTypeId"] objectForKey:@"text"];
            property.propertyLocationId=[[item objectForKey:@"PropertyLocationId"] objectForKey:@"text"];
            property.propertyName=[[item objectForKey:@"PropertyName"] objectForKey:@"text"];
            
            property.geoLat=[[[item objectForKey:@"GeoLat"] objectForKey:@"text"] doubleValue];
            property.geoLng=[[[item objectForKey:@"GeoLat"] objectForKey:@"text"] doubleValue];
            property.price=[[[item objectForKey:@"Price"] objectForKey:@"text"] doubleValue];
            property.maxBid=[[[item objectForKey:@"MaxBid"] objectForKey:@"text"] doubleValue];
            property.userName=[[item objectForKey:@"UserName"] objectForKey:@"text"];
            property.publisherName=[[item objectForKey:@"PublisherName"] objectForKey:@"text"];
            property.requestType=[[item objectForKey:@"RequestType"] objectForKey:@"text"];
            property.rentPriceDuration=[[item objectForKey:@"RentPriceDuration"] objectForKey:@"text"];
            property.rooms=[[[item objectForKey:@"Rooms"] objectForKey:@"text"]integerValue];
            property.area=[[[item objectForKey:@"Area"] objectForKey:@"text"] doubleValue];
            property.floors=[[[item objectForKey:@"Floors"] objectForKey:@"text"] integerValue];
            property.views=[[[item objectForKey:@"Views"] objectForKey:@"text"] integerValue];
            property.units=[[[item objectForKey:@"Units"] objectForKey:@"text"] integerValue];
            property.bathrooms=[[[item objectForKey:@"Bathrooms"] objectForKey:@"text"] integerValue];
            property.creationDate=[[item objectForKey:@"CreationDate"] objectForKey:@"text"];
            property.isFavorited=(BOOL)[item objectForKey:@"IsFavorited"];
            property.isFollowed=(BOOL)[item objectForKey:@"IsFollowed"];
            property.thumbURL=[[item objectForKey:@"ThumbUrl"] objectForKey:@"text"];
            property.publisherImageUrl=[[item objectForKey:@"PublisherImageUrl"] objectForKey:@"text"];
            [_propertiesList addObject:property];
        }
        [mProces removeFromSuperview];
        
        [_propertiesTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        
        //  [self performSegueWithIdentifier:@"showlogin" sender:self];
        [[AppManager sharedManager]setHomeList:_propertiesList];
        [UIView animateWithDuration:0.3 animations:^{
            _filterView.alpha = 0;
        } completion: ^(BOOL finished) {
            
            _filterView.hidden = finished;
        }];
       
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        
        //NSLog(@"Failed %@",error);
        [mProces removeFromSuperview];
        [self fetchData];
    }];
    
    [operation start];
    
}

- (IBAction)dismisssearchfilter:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        _filterView.alpha = 0;
    } completion: ^(BOOL finished) {
        
        _filterView.hidden = finished;
    }];
}

- (IBAction)dismisskey:(id)sender {
    [_type resignFirstResponder];
    [_fromprice resignFirstResponder];
        [_toprice resignFirstResponder];
    [_locations resignFirstResponder];
    [_keywordfield resignFirstResponder];
    [_rentduration resignFirstResponder];
}
- (IBAction)forsaleaction:(id)sender {
    if ([sender isSelected]) {
    _forsale.selected=NO;
    }else {
    _forsale.selected=YES;
    }

}

- (IBAction)askingforsale:(id)sender {
    if ([sender isSelected]) {
        _asksale.selected=NO;
    }else {
        _asksale.selected=YES;
    }
}

- (IBAction)forrentaction:(id)sender {
    if ([sender isSelected]) {
        _forrent.selected=NO;
    }else {
        _forrent.selected=YES;
    }
}

- (IBAction)askforrentaction:(id)sender {
    if ([sender isSelected]) {
        _askrent.selected=NO;
    }else {
        _askrent.selected=YES;
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        
        [_locationManager stopUpdatingLocation];
        
    }
    
}
@end
