//
//  ContactViewController.m
//  3akaratya
//
//  Created by mhGaber on 8/3/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "ContactViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import <QuartzCore/QuartzCore.h>
@implementation ContactViewController

-(void)viewDidLoad{
    _messagecontent.layer.borderColor = [UIColor blackColor].CGColor;
    _messagecontent.layer.borderWidth = 1.0f;
    
    
    _sendsmsbtn.layer.cornerRadius = 5;
    _sendsmsbtn.layer.masksToBounds = YES;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Add a shadow to the top view so it looks like it is on top of the others
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [[UIColor blackColor] CGColor];
    
    // Tell it which view should be created under left
    if (![self.slidingViewController.underRightViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    
    // Add the pan gesture to allow sliding
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    [self.navigationController.navigationBar setHidden:YES ];
}

- (IBAction)showmenuaction:(id)sender {
    if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered){
        [self.slidingViewController anchorTopViewToLeftAnimated:YES];
    }
    else{
        [self.slidingViewController resetTopViewAnimated:YES];
    }
}
- (IBAction)sendsmsaction:(id)sender {
}

- (IBAction)backaction:(id)sender {
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeid"];
   // [self.slidingViewController resetTopViewAnimated:YES];
}
@end
