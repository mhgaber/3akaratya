//
//  MessageModel.h
//  3akaratya
//
//  Created by mhGaber on 9/9/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageModel : NSObject
@property(strong,nonatomic) NSString *id_message;
@property(strong,nonatomic)NSString *subject;
@property(strong,nonatomic) NSString *date;
@property(strong,nonatomic) NSString *senderFullName;
@property(strong,nonatomic) NSString *senderUserName;
@property(strong,nonatomic)NSString *receiveFullName;
@property(strong,nonatomic) NSString *receiveUserName;
@property(nonatomic)BOOL isOpened;


@end
