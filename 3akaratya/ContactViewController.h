//
//  ContactViewController.h
//  3akaratya
//
//  Created by mhGaber on 8/3/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactViewController : UITableViewController
- (IBAction)showmenuaction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *messagecontent;

@property (weak, nonatomic) IBOutlet UIButton *sendsmsbtn;
- (IBAction)sendsmsaction:(id)sender;

- (IBAction)backaction:(id)sender;
@end
