//
//  ThumbRow.h
//  3akaratya
//
//  Created by mhGaber on 7/7/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThumbRow : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *propertyRooms;
@property (weak, nonatomic) IBOutlet UILabel *propertyArea;
@property (weak, nonatomic) IBOutlet UILabel *propertytitlethumb;

@property (weak, nonatomic) IBOutlet UILabel *propertyUnits;

@property (weak, nonatomic) IBOutlet UILabel *propertyViews;
@property (weak, nonatomic) IBOutlet UILabel *propertyID;
@property (weak, nonatomic) IBOutlet UILabel *propertyPrice;

@property (weak, nonatomic) IBOutlet UIImageView *publisherimage;
@property (weak, nonatomic) IBOutlet UILabel *propertyDate;
@property (weak, nonatomic) IBOutlet UILabel *datetitle;
@property (weak, nonatomic) IBOutlet UILabel *roomstitle;
@property (weak, nonatomic) IBOutlet UILabel *viewstitle;
@property (weak, nonatomic) IBOutlet UILabel *unitstitle;
@property (weak, nonatomic) IBOutlet UILabel *areatitle;


@end
