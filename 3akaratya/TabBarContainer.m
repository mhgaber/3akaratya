//
//  TabBarContainer.m
//  3akaratya
//
//  Created by mhGaber on 8/2/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "TabBarContainer.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
@implementation TabBarContainer


@synthesize centerButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UITabBar *tabBar = self.tabBar;
    self.selectedViewController=[self.viewControllers objectAtIndex:4];//or whichever index you want

    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
   UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:3];
        UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
  //      [self addLeft:[[UIImage imageNamed:@"accounttabicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] highlightImage:[[UIImage imageNamed:@"accounttabicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] target:self action:@selector(buttonPressed:)];
  //  UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
//    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:5];
    tabBarItem1.title = @"المفضله";
    tabBarItem2.title = @"رسائل";
    tabBarItem3.title = @"لوحتي";
  //  tabBarItem4.title = @"البريد";
    tabBarItem5.title = @"الرئيسيه";
    
//    [tabBarItem1 setFinishedSelectedImage:[UIImage imageNamed:@"hometabicon"] withFinishedUnselectedImage:[UIImage imageNamed:@"hometabicon"]];
    
  //  [tabBarItem1 setImage:[[UIImage imageNamed:@"hometabicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];

        [tabBarItem2 setImage:[[UIImage imageNamed:@"messagetabicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//           [tabBarItem3 setImage:[[UIImage imageNamed:@"accounttabicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem2 setBadgeValue:@"1"];

    // Change the tab bar background
    UIImage* tabBarBackground = [UIImage imageNamed:@"tabbar.png"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"tabbar_selected.png"]];
    
    // Change the title color of tab bar items
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor whiteColor], UITextAttributeTextColor,
                                                       nil] forState:UIControlStateNormal];
    UIColor *titleHighlightedColor = [UIColor colorWithRed:153/255.0 green:192/255.0 blue:48/255.0 alpha:1.0];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       titleHighlightedColor, UITextAttributeTextColor,
                                                       nil] forState:UIControlStateHighlighted];
    
    
        UIView *tabb=[self viewForTabBarItemAtIndex:3   ];
    CGRect fr=[self frameForTabInTabBar:self.tabBar withIndex:3];
    UIImage *buttonImage = [UIImage imageNamed:@"accounttabicon"];
    UIImage *highlightImage = [UIImage imageNamed:@"accounttabicon"];
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(fr.origin.x+10   ,fr.origin.y,fr.size.width-19, 40);
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    //[button setBackgroundColor:[UIColor colorWithRed:62.0/255.0 green:49.0/255.0 blue:50.0/255.0 alpha:1.0 ]];
//    button.center=   tabb.center;
    [button addTarget:self action:@selector(openMenu:) forControlEvents:UIControlEventTouchUpInside];
    [self.tabBar addSubview:button];
     CGRect fr3=[self frameForTabInTabBar:self.tabBar withIndex:2];
    buttonImage = [UIImage imageNamed:@"publishicontab"];
    highlightImage = [UIImage imageNamed:@"publishicontab"];
    UIButton *buttonn = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonn.frame = CGRectMake(fr3.origin.x   ,fr3.origin.y,fr3.size.width, fr3.size.height);
    [buttonn setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [buttonn setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    
    [buttonn addTarget:self action:@selector(showMessage:) forControlEvents:UIControlEventTouchUpInside];
    [self.tabBar addSubview:buttonn];
    //[self addCenterButtonWithImage:[[UIImage imageNamed:@"publishicontab"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] highlightImage:[[UIImage imageNamed:@"publishicontab"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] target:self action:@selector(buttonPressed:)];

}
- (void)addCenterButtonWithImage:(UIImage *)buttonImage highlightImage:(UIImage *)highlightImage target:(id)target action:(SEL)action
{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    
    CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
    if (heightDifference < 0) {
        button.center = self.tabBar.center;
    } else {
        CGPoint center = self.tabBar.center;
        center.y = center.y - heightDifference/2.0;
        button.center = center;
    }
    
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    self.centerButton = button;
}

-(void)showMessage:(id)sender{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"مشاركه" message:@"تحت الانشاء" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil];
    [alert show];
}
- (CGRect)frameForTabInTabBar:(UITabBar*)tabBar withIndex:(NSUInteger)index
{
    NSMutableArray *tabBarItems = [NSMutableArray arrayWithCapacity:[tabBar.items count]];
    for (UIView *view in tabBar.subviews) {
        if ([view isKindOfClass:NSClassFromString(@"UITabBarButton")] && [view respondsToSelector:@selector(frame)]) {
            // check for the selector -frame to prevent crashes in the very unlikely case that in the future
            // objects thar don't implement -frame can be subViews of an UIView
            [tabBarItems addObject:view];
        }
    }
    if ([tabBarItems count] == 0) {
        // no tabBarItems means either no UITabBarButtons were in the subView, or none responded to -frame
        // return CGRectZero to indicate that we couldn't figure out the frame
        return CGRectZero;
    }
    
    // sort by origin.x of the frame because the items are not necessarily in the correct order
    [tabBarItems sortUsingComparator:^NSComparisonResult(UIView *view1, UIView *view2) {
        if (view1.frame.origin.x < view2.frame.origin.x) {
            return NSOrderedAscending;
        }
        if (view1.frame.origin.x > view2.frame.origin.x) {
            return NSOrderedDescending;
        }
        NSAssert(NO, @"%@ and %@ share the same origin.x. This should never happen and indicates a substantial change in the framework that renders this method useless.", view1, view2);
        return NSOrderedSame;
    }];
    
    CGRect frame = CGRectZero;
    if (index < [tabBarItems count]) {
        // viewController is in a regular tab
        UIView *tabView = tabBarItems[index];
        if ([tabView respondsToSelector:@selector(frame)]) {
            frame = tabView.frame;
        }
    }
    else {
        // our target viewController is inside the "more" tab
        UIView *tabView = [tabBarItems lastObject];
        if ([tabView respondsToSelector:@selector(frame)]) {
            frame = tabView.frame;
        }
    }
    return frame;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

// Create a custom UIButton and add it to the center of our tab bar

-(UIView*)viewForTabBarItemAtIndex:(NSInteger)index {
    
    CGRect tabBarRect = self.tabBarController.tabBar.frame;
    NSInteger buttonCount = self.tabBarController.tabBar.items.count;
    CGFloat containingWidth = tabBarRect.size.width/buttonCount;
    CGFloat originX = containingWidth * index ;
    CGRect containingRect = CGRectMake( originX, 0, containingWidth, self.tabBarController.tabBar.frame.size.height );
    CGPoint center = CGPointMake( CGRectGetMidX(containingRect), CGRectGetMidY(containingRect));
    return [ self.tabBarController.tabBar hitTest:center withEvent:nil ];
}
-(void)openMenu:(id)sender{
    if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered){
        [self.slidingViewController anchorTopViewToLeftAnimated:YES];
    }
    else{
        [self.slidingViewController resetTopViewAnimated:YES];
    }
    
}



- (void)doHighlight:(UIButton*)b {
    [b setHighlighted:YES];
}

- (void)doNotHighlight:(UIButton*)b {
    [b setHighlighted:NO];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    if(self.tabBarController.selectedIndex != 2){
        [self performSelector:@selector(doNotHighlight:) withObject:centerButton afterDelay:0];
    }
}

- (BOOL)tabBarHidden {
    return self.centerButton.hidden && self.tabBar.hidden;
}

- (void)setTabBarHidden:(BOOL)tabBarHidden
{
    self.centerButton.hidden = tabBarHidden;
    self.tabBar.hidden = tabBarHidden;
}

@end
