//
//  FollowingModel.h
//  3akaratya
//
//  Created by mhGaber on 8/16/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FollowingModel : NSObject
@property(strong,nonatomic)NSString *userName;
@property(strong,nonatomic)NSString *tradeName;
@property(strong,nonatomic) NSString *thumbURL;
@end
