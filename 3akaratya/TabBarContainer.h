//
//  TabBarContainer.h
//  3akaratya
//
//  Created by mhGaber on 8/2/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarContainer : UITabBarController

@property(nonatomic, weak) IBOutlet UIButton *centerButton;
@property(nonatomic, weak) IBOutlet UIButton *leftButton;

@property(nonatomic, assign) BOOL tabBarHidden;

-(void)addCenterButtonWithImage:(UIImage *)buttonImage highlightImage:(UIImage *)highlightImage target:(id)target action:(SEL)action;

@end
