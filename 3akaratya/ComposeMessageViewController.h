//
//  ComposeMessageViewController.h
//  3akaratya
//
//  Created by mhGaber on 9/16/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPGTextField.h"
@interface ComposeMessageViewController : UITableViewController<MPGTextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextView *messagebody;

@property (weak, nonatomic) IBOutlet MPGTextField *sendtofield;

@property (weak, nonatomic) IBOutlet UITextField *subjectmessagefield;
@property (weak, nonatomic) IBOutlet UILabel *subjectmessagetitle;
@property (weak, nonatomic) IBOutlet UILabel *sendtotitle;
- (IBAction)backbtnact:(id)sender;

- (IBAction)sendbtnac:(id)sender;



@end
