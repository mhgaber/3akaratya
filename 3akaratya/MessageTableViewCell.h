//
//  MessageTableViewCell.h
//  3akaratya
//
//  Created by mhGaber on 8/2/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *messagethumb;
@property (weak, nonatomic) IBOutlet UILabel *messagetitle;
@property (weak, nonatomic) IBOutlet UILabel *messagesender;
@property (weak, nonatomic) IBOutlet UILabel *messagedate;

@end
