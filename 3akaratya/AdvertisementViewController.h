//
//  AdvertisementViewController.h
//  3akaratya
//
//  Created by mhGaber on 8/3/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@interface AdvertisementViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate>

- (IBAction)showmenu:(id)sender;
@property(strong,nonatomic) NSMutableArray *advertisementArr;

@property (weak, nonatomic) IBOutlet UITableView *advertisingtableview;
- (IBAction)backbtnaction:(id)sender;

@end
