//
//  ViewController.h
//  3akaratya
//
//  Created by mhGaber on 7/5/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "Property.h"
#import <CoreLocation/CLLocationManager.h>
#import <CoreLocation/CoreLocation.h>
#import "IQDropDownTextField.h"
@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *propertiesTable;

- (IBAction)openMenu:(id)sender;
@property(strong,nonatomic) NSMutableArray *requestsTypesArr;
@property(strong,nonatomic) NSMutableArray *locationsdefined;

@property(strong,nonatomic) NSMutableArray *propertiesList;
@property(strong,nonatomic) Property *selectedProperty;
@property (weak, nonatomic) IBOutlet UITextField *usernametext;
@property (weak, nonatomic) IBOutlet UITextField *passwordtext;
@property (weak, nonatomic) IBOutlet UITextField *emailtext;
@property (weak, nonatomic) IBOutlet UIButton *regorloginbtn;
- (IBAction)regorloginbtn:(id)sender;
- (IBAction)closeaction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *logorregview;
@property (weak, nonatomic) IBOutlet UIView *filterView;

-(void)showLogger;
@property (weak, nonatomic) IBOutlet IQDropDownTextField *locations;
@property (weak, nonatomic) IBOutlet UITextField *fromprice;
@property (weak, nonatomic) IBOutlet UITextField *toprice;

@property (weak, nonatomic) IBOutlet IQDropDownTextField *type;

@property (weak, nonatomic) IBOutlet UITextField *keywordfield;
@property (weak, nonatomic) IBOutlet IQDropDownTextField *rentduration;
- (IBAction)searchproperties:(id)sender;

- (IBAction)dismisssearchfilter:(id)sender;

- (IBAction)dismisskey:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *forsale;
@property (weak, nonatomic) IBOutlet UIButton *asksale;
@property (weak, nonatomic) IBOutlet UIButton *forrent;
@property (weak, nonatomic) IBOutlet UIButton *askrent;
@property (weak, nonatomic) IBOutlet UIButton *searchfilterbtn;

- (IBAction)forsaleaction:(id)sender;
- (IBAction)askingforsale:(id)sender;
- (IBAction)forrentaction:(id)sender;

- (IBAction)askforrentaction:(id)sender;
@property(strong,nonatomic) NSMutableArray *locationsIDs;
@property(strong,nonatomic) NSMutableArray *locationsArr;


@property(strong,nonatomic)   CLLocationManager *locationManager;
@property(nonatomic) double lat;
@property(nonatomic) double lon;

@end

