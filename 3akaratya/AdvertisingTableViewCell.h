//
//  AdvertisingTableViewCell.h
//  3akaratya
//
//  Created by mhGaber on 8/3/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "SWTableViewCell.h"

@interface AdvertisingTableViewCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *advtext;

@end
