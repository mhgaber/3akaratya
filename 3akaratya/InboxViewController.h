//
//  InboxViewController.h
//  3akaratya
//
//  Created by mhGaber on 8/2/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InboxViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *messagesTableView;
@property(strong,nonatomic) NSMutableArray *messagesArray;
@property (weak, nonatomic) IBOutlet UIButton *inboxbtn;
@property (weak, nonatomic) IBOutlet UIButton *sentbtn;
- (IBAction)inboxaction:(id)sender;
- (IBAction)sentaction:(id)sender;

@end
