//
//  PropertyDetailsModel.h
//  3akaratya
//
//  Created by mhGaber on 7/30/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PropertyDetailsModel : NSObject
@property(nonatomic) int propertyID;
@property(nonatomic) BOOL isClosed;
@property(strong,nonatomic) NSString *propertyTypeID;
@property(nonatomic) int propertyLocationID;
@property(strong,nonatomic) NSString *propertyName;
@property(strong,nonatomic) NSString *propertyDescription;
@property(strong,nonatomic) NSString *propertyNeighborhoodDescription;
@property(strong,nonatomic) NSMutableArray *propertyUtitlties;
@property(nonatomic) double lat;
@property(nonatomic) double lon;
@property(nonatomic) double price;
@property(strong,nonatomic) NSString *currency;
@property(strong,nonatomic) NSString *userName;
@property(strong,nonatomic) NSString *publisherName;
@property(strong,nonatomic) NSString *requesttype;
@property(nonatomic) int rentPriceDuration;
@property(nonatomic) int rooms;
@property(nonatomic) double area;
@property(nonatomic) int floors;
@property(nonatomic) int units;
@property(nonatomic) int views;
@property(nonatomic) int baths;
@property(strong,nonatomic) NSString *creationDate;
@property(nonatomic) BOOL isFavorited;
@property(nonatomic) BOOL isFollowed;
@property(strong,nonatomic) NSString *thumbURL;
@property(strong,nonatomic) NSMutableArray *propertyPhotos;
@property(strong,nonatomic) NSString *publisherThumb;
@property(strong,nonatomic) NSString *locationName; 

@property(strong,nonatomic) NSString *agentTitle;
@property(strong,nonatomic) NSString *agentAdress;
@property(strong,nonatomic) NSString *agentPhone;
@property(strong,nonatomic) NSString *agentWorkPhone;
@property(strong,nonatomic) NSString *agentMobile;
@property(strong,nonatomic) NSString *agentCountry;
@property(strong,nonatomic) NSString *agentCity;

@end
