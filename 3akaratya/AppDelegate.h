//
//  AppDelegate.h
//  3akaratya
//
//  Created by mhGaber on 7/5/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

