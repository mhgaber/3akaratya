//
//  MyAccountViewController.m
//  3akaratya
//
//  Created by mhGaber on 8/21/15.
//  Copyright (c) 2015 gaber. All rights reserved.
//

#import "MyAccountViewController.h"
#import <QBImagePickerController/QBImagePickerController.h>
#import <QuartzCore/QuartzCore.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "AppManager.h"
#import "LoginCustomViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"

@implementation MyAccountViewController

-(void)viewDidLoad{
    _savedatabutton.layer.cornerRadius = 5;
    _savedatabutton.layer.masksToBounds = YES;
}

- (IBAction)chooseImagePicker:(id)sender {

    
  
    QBImagePickerController *imagePickerController = [QBImagePickerController new]; imagePickerController.delegate = self;
    imagePickerController.minimumNumberOfSelection = 1;
    imagePickerController.maximumNumberOfSelection = 1;

    imagePickerController.showsNumberOfSelectedAssets = YES;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];

}

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets {
  

    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)saveData:(id)sender {
    if(_mobileno.text.length==0)
        _mobileno.text=@"";
    
    if(_fullname.text.length>0&&_addressone.text.length>0&&_addresstwo.text.length>0&&_city.text.length>0&&_country.text.length>0&&_postalcode.text.length>0&&_phone.text.length>0&&_workphone.text.length>0&&_faxno.text.length>0){

        NSURL *url = [NSURL URLWithString:@"http://dev.info-est.com/Services/Mobile.asmx"];
        
        NSString *soapBody =[[NSString alloc]initWithFormat: @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:info=\"http://info-est.com/\"><soapenv:Header/><soapenv:Body><info:UpdateAccountContactDetails><info:AuthToken>%@</info:AuthToken><info:FullName>%@</info:FullName><info:AddressLine1>%@</info:AddressLine1><info:AddressLine2>%@</info:AddressLine2><info:City>%@</info:City><info:CountryISO3>%@</info:CountryISO3><info:PostalCode>%@</info:PostalCode><info:Phone>%@</info:Phone><info:WorkPhone>%@</info:WorkPhone><info:MobileNumber>%@</info:MobileNumber><info:Fax>%@</info:Fax></info:UpdateAccountContactDetails></soapenv:Body></soapenv:Envelope>",[[AppManager sharedManager]accessToken],_fullname.text,_addressone.text,_addresstwo.text,_city.text,_country.text,_postalcode.text,_phone.text,_workphone.text,_mobileno.text,_faxno.text];
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPMethod:@"POST"]; //GET
        
        [request addValue:@"http://info-est.com/UpdateAccountContactDetails" forHTTPHeaderField:@"SOAPAction"];
        
        [request addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        MBProgressHUD *mProces=[[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:mProces];
        mProces.animationType=MBProgressHUDAnimationFade;
        mProces.color=[UIColor colorWithRed:83.0/255.0 green:110.0/255.0 blue:85.0/255.0 alpha:1.0];
        [mProces show:YES];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
            
            NSLog(@"Success : Content : %@",responseObject);
            NSLog(@"Success : Content : %@",[operation responseString]);
            [mProces removeFromSuperview];
        } failure:^(AFHTTPRequestOperation *operation,NSError *error){
            
            //NSLog(@"Failed %@",error);
            [mProces removeFromSuperview];
            
        }];
        
        [operation start];
    
    }else {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"تحذير" message:@"يجب ملئ جميع الحقول" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil];
        [alert show];
    }
}
- (IBAction)backbtnaction:(id)sender {
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeid"];
//    [self.slidingViewController resetTopViewAnimated:YES];
}
@end
